package ru.mirea.petrakov.ourtour.domain.contacts.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;

public class IncreaseReviewRatingUseCase {
    private static final String TAG = "Increase Review Rating";
    private final ReviewRepository reviewRepository;

    public IncreaseReviewRatingUseCase(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public void execute(String reviewId) {
        Review review = reviewRepository.getReviewById(reviewId);

        if (review != null) {
            reviewRepository.increaseReviewRating(review);
            Log.d(TAG, "Increased helpful rating for review: " + reviewId);
        } else {
            Log.e(TAG, "Review with ID " + reviewId + " not found.");
        }
    }
}

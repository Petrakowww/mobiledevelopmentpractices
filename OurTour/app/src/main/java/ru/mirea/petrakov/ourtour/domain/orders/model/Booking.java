package ru.mirea.petrakov.ourtour.domain.orders.model;

import java.time.LocalDateTime;

public class Booking {
    private String bookingId;
    private String tourId;
    private String userId;
    private LocalDateTime bookingTime;
    private BookingStatus status;
    private int bookedSeats; // Количество забронированных мест
    private int availableSeats; // Количество доступных мест

    public Booking(String bookingId, String tourId, String userId, LocalDateTime bookingTime, BookingStatus status, int bookedSeats, int availableSeats) {
        this.bookingId = bookingId;
        this.tourId = tourId;
        this.userId = userId;
        this.bookingTime = bookingTime;
        this.status = status;
        this.bookedSeats = bookedSeats;
        this.availableSeats = availableSeats;
    }

    public String getBookingId() {
        return bookingId;
    }

    public String getTourId() {
        return tourId;
    }

    public String getUserId() {
        return userId;
    }

    public LocalDateTime getBookingTime() {
        return bookingTime;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public int getBookedSeats() {
        return bookedSeats;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setBookedSeats(int bookedSeats) {
        this.bookedSeats = bookedSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public void setSeatsToBook(int seatsToBook) {
        if (seatsToBook <= availableSeats && seatsToBook >= 0) {
            setBookedSeats(seatsToBook + getBookedSeats());
            setAvailableSeats(availableSeats - seatsToBook);
        }
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookingId='" + bookingId + '\'' +
                ", tourId='" + tourId + '\'' +
                ", userId='" + userId + '\'' +
                ", bookingTime=" + bookingTime +
                ", status=" + status +
                ", bookedSeats=" + bookedSeats +
                ", availableSeats=" + availableSeats +
                '}';
    }
}

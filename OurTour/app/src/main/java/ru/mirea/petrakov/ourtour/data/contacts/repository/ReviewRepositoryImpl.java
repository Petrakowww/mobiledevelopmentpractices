package ru.mirea.petrakov.ourtour.data.contacts.repository;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;

public class ReviewRepositoryImpl implements ReviewRepository {
    private static final String TAG = "ReviewRepositoryImpl"; // For logging
    private final List<Review> reviews;

    public ReviewRepositoryImpl() {
        this.reviews = new ArrayList<>();
    }

    @Override
    public void addReview(Review review) {
        reviews.add(review);
        Log.d(TAG, "Added review: " + review);
    }

    @Override
    public Review getReviewById(String reviewId) {
        return reviews.stream()
                .filter(review -> review.getReviewId().equals(reviewId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void replyToReview(Review review, String reply) {
        Review foundReview = reviews.stream()
                .filter(r -> r.getReviewId().equals(review.getReviewId())) // Change from bookingId to reviewId
                .findFirst()
                .orElse(null);

        if (foundReview != null) {
            if (foundReview.getReply() == null) {
                foundReview.setReply(new ArrayList<>());
            }
            foundReview.getReply().add(reply);
            Log.d(TAG, "Replied to review with ID " + review.getReviewId() + ": " + reply);
        } else {
            Log.e(TAG, "Review with ID " + review.getReviewId() + " not found.");
        }
    }

    @Override
    public void increaseReviewRating(Review review) {
        Review foundReview = reviews.stream()
                .filter(r -> r.getReviewId().equals(review.getReviewId()))
                .findFirst()
                .orElse(null);

        if (foundReview != null){
            foundReview.setHelpfulRating(foundReview.getHelpfulRating() + 1);
        }
        else {
            Log.e(TAG, "Review with ID " + review.getReviewId() + " not found.");
        }
    }

    @Override
    public void editReview(Review destinationReview) {
        for (Review existingReview : reviews) {
            if (existingReview.getReviewId().equals(destinationReview.getReviewId())) {
                existingReview.setComment(destinationReview.getComment());
                existingReview.setRating(destinationReview.getRating());
                existingReview.setPhotoUrls(destinationReview.getPhotoUrls());
                existingReview.setReply(destinationReview.getReply());

                Log.d(TAG, "Edited review: " + existingReview);
                return;
            }
        }
        Log.e(TAG, "Review with ID " + destinationReview.getReviewId() + " not found.");
    }

    @Override
    public void deleteReview(Review review) {
        boolean removed = reviews.removeIf(r -> r.getReviewId().equals(review.getReviewId()));
        if (removed) {
            Log.d(TAG, "Deleted review with ID " + review.getReviewId());
        } else {
            Log.e(TAG, "Couldn't delete review with ID " + review.getReviewId() + ", review not found.");
        }
    }

    @Override
    public List<Review> getAllReviews() {
        return new ArrayList<>(reviews);
    }
}

package ru.mirea.petrakov.ourtour.presentation.contacts;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.mirea.petrakov.ourtour.data.contacts.repository.ReviewRepositoryImpl;
import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.AddReviewUseCase;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.DeleteReviewByIdUseCase;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.EditReviewUseCase;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.GetAllReviewsUseCase;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.GetReviewByIdUseCase;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.IncreaseReviewRatingUseCase;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.ReplyReviewUseCase;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.validators.ReviewValidator;
import ru.mirea.petrakov.ourtour.presentation.SystemTests;

public class TestContactSystem implements SystemTests {
    @Override
    public void RunTestSystem(String logTag) {
        ReviewRepositoryImpl reviewRepository = new ReviewRepositoryImpl();
        ReviewValidator reviewValidator = new ReviewValidator();

        GetAllReviewsUseCase getAllReviewsUseCase = new GetAllReviewsUseCase(reviewRepository);
        GetReviewByIdUseCase getReviewByIdUseCase = new GetReviewByIdUseCase(reviewRepository);
        AddReviewUseCase addReviewUseCase = new AddReviewUseCase(reviewValidator, reviewRepository);
        ReplyReviewUseCase replyReviewUseCase = new ReplyReviewUseCase(reviewRepository);
        DeleteReviewByIdUseCase deleteReviewUseCase = new DeleteReviewByIdUseCase(reviewRepository);
        EditReviewUseCase editReviewUseCase = new EditReviewUseCase(reviewValidator, reviewRepository);
        IncreaseReviewRatingUseCase increaseReviewRatingUseCase = new IncreaseReviewRatingUseCase(reviewRepository);

        List<String> photoUrls = new ArrayList<>();
        photoUrls.add("a.jpg");
        photoUrls.add("b.jpg");

        Review review1 = new Review("review1", "booking1", 1,
                "Good!", photoUrls);
        Review review2 = new Review("review2", "booking3", 4,
                "Good!", photoUrls);
        Review review3 = new Review("review3", "booking4", 3,
                "Good!", photoUrls);

        addReviewUseCase.execute(review1);
        addReviewUseCase.execute(review2);
        addReviewUseCase.execute(review3);

        Log.d(logTag, "All reviews: " + getAllReviewsUseCase.execute());

        Log.d(logTag, "Get review by id:" + getReviewByIdUseCase.execute("review1"));
        replyReviewUseCase.execute(review2, "Nice!");
        Log.d(logTag, "Get review by id:" + getReviewByIdUseCase.execute("review2"));

        deleteReviewUseCase.execute("review3");
        Log.d(logTag, "All reviews: " + getAllReviewsUseCase.execute());

        editReviewUseCase.execute(new Review("review1", "booking1", 3,
                "Not bad!", new ArrayList<>()));

        Log.d(logTag, "Edit review: " + getReviewByIdUseCase.execute("review1"));

        increaseReviewRatingUseCase.execute("review1");

        Log.d(logTag, "Increase review: " + getReviewByIdUseCase.execute("review1"));
    }
}

package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import java.util.List;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class GetAllToursUseCase {
    private final TourRepository tourRepository;

    public GetAllToursUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public List<Tour> execute() {
        return tourRepository.getAllTours();
    }
}

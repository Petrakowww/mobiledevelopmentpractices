package ru.mirea.petrakov.ourtour.domain.account.model;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;

public interface ClientFactory<T extends Client> {
    T createClient(String username, String password, String email, String phone);
}

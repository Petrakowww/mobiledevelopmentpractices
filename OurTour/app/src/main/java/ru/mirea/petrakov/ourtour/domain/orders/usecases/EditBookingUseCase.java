package ru.mirea.petrakov.ourtour.domain.orders.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;
import ru.mirea.petrakov.ourtour.domain.orders.services.BookingAutoCancelService;

public class EditBookingUseCase {
    private final BookingRepository bookingRepository;
    private final BookingAutoCancelService bookingAutoCancelService;

    public EditBookingUseCase(BookingRepository bookingRepository,
                              BookingAutoCancelService bookingAutoCancelService) {
        this.bookingRepository = bookingRepository;
        this.bookingAutoCancelService = bookingAutoCancelService;
    }

    public void execute(Booking booking, long newCancelAfterSeconds) {
        Booking existingBooking = bookingRepository.getBookingById(booking.getBookingId());

        if (existingBooking != null) {
            if (existingBooking.getStatus() == BookingStatus.PENDING) {
                Log.d("EditBooking", "Booking " + booking.getBookingId() + " is being modified. Resetting auto-cancel timer.");
                bookingAutoCancelService.stopAutoCancel();
                bookingAutoCancelService.scheduleAutoCancel(booking, newCancelAfterSeconds);
            } else {
                Log.d("EditBooking", "Booking " + booking.getBookingId() + " is confirmed. Modifying without resetting timer.");
            }
            bookingRepository.editBooking(booking);
        } else {
            Log.e("EditBooking", "Booking not found with ID: " + booking.getBookingId());
        }
    }
}

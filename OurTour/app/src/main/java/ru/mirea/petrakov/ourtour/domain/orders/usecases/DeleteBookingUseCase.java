package ru.mirea.petrakov.ourtour.domain.orders.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;
import ru.mirea.petrakov.ourtour.domain.orders.services.BookingAutoCancelService;

public class DeleteBookingUseCase {
    private final BookingRepository bookingRepository;
    private final BookingAutoCancelService bookingAutoCancelService;

    public DeleteBookingUseCase(BookingRepository bookingRepository,
                                BookingAutoCancelService bookingAutoCancelService) {
        this.bookingRepository = bookingRepository;
        this.bookingAutoCancelService = bookingAutoCancelService;
    }

    public void execute(String bookingId) {
        Booking booking = bookingRepository.getBookingById(bookingId);

        if (booking != null) {
            if (booking.getStatus() == BookingStatus.PENDING) {
                Log.d("DeleteBooking", "Booking " + bookingId + " is pending, stopping auto-cancel and changing status to Canceled.");
                bookingAutoCancelService.stopAutoCancel();
                bookingRepository.changeBookingStatus(bookingId, BookingStatus.CANCELED);
            } else {
                Log.d("DeleteBooking", "Booking " + bookingId + " is confirmed. Changing status to Canceled.");
                bookingRepository.changeBookingStatus(bookingId, BookingStatus.CANCELED);
            }
            bookingRepository.deleteBooking(bookingId);
        } else {
            Log.e("DeleteBooking", "Booking not found with ID: " + bookingId);
        }
    }
}

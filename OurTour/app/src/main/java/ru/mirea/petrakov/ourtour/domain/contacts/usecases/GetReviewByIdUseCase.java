package ru.mirea.petrakov.ourtour.domain.contacts.usecases;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;

public class GetReviewByIdUseCase {
    private final ReviewRepository reviewRepository;

    public GetReviewByIdUseCase(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public Review execute(String id) {
        return reviewRepository.getReviewById(id);
    }
}

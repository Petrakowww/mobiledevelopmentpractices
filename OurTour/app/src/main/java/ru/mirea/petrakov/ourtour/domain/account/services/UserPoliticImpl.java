package ru.mirea.petrakov.ourtour.domain.account.services;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;

public class UserPoliticImpl implements ClientPolitic<User> {
    @Override
    public void politicBanned(User user, boolean flag) {
        user.setBlock(flag);
    }

    @Override
    public void politicChangedData(User user, String key, String value) {
        switch (key) {
            case "email":
                user.setEmail(value);
                break;
            case "username":
                user.setUsername(value);
                break;
            case "phone":
                user.setPhoneNumber(value);
                break;
            default:
        }
    }
}

package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class DeleteTourUseCase {
    private final TourRepository tourRepository;

    public DeleteTourUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public void execute(String tourId) {
        Tour tourToDelete = tourRepository.getTourById(tourId);
        if (tourToDelete != null) {
            tourRepository.getAllTours().remove(tourToDelete);
        }
    }
}

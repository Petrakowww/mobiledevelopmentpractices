package ru.mirea.petrakov.ourtour.domain.account.usecases.valid;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;

public interface AccountValidate<T extends Client> {
    boolean validateUsername(String username);
    boolean validatePassword(String password);
    boolean validateEmail(String email);
    boolean validatePhone(String phone);
    boolean validateClient(T client);
    boolean validateAuth(String identifier, String password);
    boolean validateBlockStatus(T client);
    boolean authentication(String identifier, String password, T client);
}

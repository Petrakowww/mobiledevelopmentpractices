package ru.mirea.petrakov.ourtour.domain.account.usecases.action;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;
import ru.mirea.petrakov.ourtour.domain.account.services.ClientPolitic;

public class AccountBlockUseCase extends AccountAction{


    public AccountBlockUseCase(AccountRepository<User> accountRepository, ClientPolitic<User> clientPolitic) {
        super(accountRepository, clientPolitic);
    }

    public void execute(User user, boolean flag){
        clientPolitic.politicBanned(user, flag);
        accountRepository.changeUserInformation(user);
        Log.d(TAG, "User status banned: " + user.isBlock());
    }
}

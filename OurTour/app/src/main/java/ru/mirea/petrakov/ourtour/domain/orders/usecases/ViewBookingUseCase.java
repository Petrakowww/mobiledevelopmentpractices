package ru.mirea.petrakov.ourtour.domain.orders.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;

public class ViewBookingUseCase {
    private final BookingRepository bookingRepository;

    public ViewBookingUseCase(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public Booking execute(String bookingId) {
        Booking booking = bookingRepository.getBookingById(bookingId);
        if (booking != null) {
            Log.d("ViewBooking", "Booking details: " + booking);
        } else {
            Log.e("ViewBooking", "Booking not found with ID: " + bookingId);
        }
        return booking;
    }
}

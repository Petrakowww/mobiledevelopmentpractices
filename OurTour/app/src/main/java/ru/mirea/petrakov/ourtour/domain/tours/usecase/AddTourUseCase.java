package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class AddTourUseCase {
    private final TourRepository tourRepository;

    public AddTourUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public void execute(Tour newTour) {
        tourRepository.getAllTours().add(newTour);
    }
}

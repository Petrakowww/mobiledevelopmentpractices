package ru.mirea.petrakov.ourtour.domain.account.model;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Admin;

public class AdminFactoryImpl implements ClientFactory<Admin> {

    @Override
    public Admin createClient(String username, String password, String email, String phone) {
        return new Admin(username, password);
    }
}

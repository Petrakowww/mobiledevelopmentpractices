package ru.mirea.petrakov.ourtour.domain.orders.usecases;

import android.util.Log;

import java.time.LocalDateTime;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;
import ru.mirea.petrakov.ourtour.domain.orders.services.BookingAutoCancelService;

public class AddTourToBookingUseCase {
    private final static String TAG = "Add tour";
    private final BookingRepository bookingRepository;
    private final BookingAutoCancelService bookingAutoCancelService;

    public AddTourToBookingUseCase(BookingRepository bookingRepository,
                                   BookingAutoCancelService bookingAutoCancelService) {
        this.bookingRepository = bookingRepository;
        this.bookingAutoCancelService = bookingAutoCancelService;
    }

    public void execute(String bookingId, String tourId, String userId,
                        int seatsToBook, int availableSeats, long cancelAfterSeconds) {
        if (seatsToBook > availableSeats) {
            Log.e(TAG, "Not enough seats available for this tour.");
            return;
        }

        int remainingSeats = availableSeats - seatsToBook;

        Booking booking = new Booking(bookingId, tourId, userId, LocalDateTime.now(),
                BookingStatus.PENDING, seatsToBook, remainingSeats);

        bookingRepository.addBooking(booking);

        // Запускаем сервис автоудаления через n минут
        bookingAutoCancelService.scheduleAutoCancel(booking, cancelAfterSeconds);
    }
}

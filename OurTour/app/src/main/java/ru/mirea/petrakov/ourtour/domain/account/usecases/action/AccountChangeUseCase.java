package ru.mirea.petrakov.ourtour.domain.account.usecases.action;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;
import ru.mirea.petrakov.ourtour.domain.account.services.ClientPolitic;

public class AccountChangeUseCase extends AccountAction{
    public AccountChangeUseCase(AccountRepository<User> accountRepository, ClientPolitic<User> clientPolitic) {
        super(accountRepository, clientPolitic);
    }

    public void execute(User client, String key, String value){
        clientPolitic.politicChangedData(client, key, value);
        accountRepository.changeUserInformation(client);

        Log.d(TAG, "User info updated: " + key + " -> " + value);
    }
}

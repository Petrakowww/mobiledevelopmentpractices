package ru.mirea.petrakov.ourtour.domain.account.usecases.action;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.account.usecases.valid.AccountValidate;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;
import ru.mirea.petrakov.ourtour.domain.account.usecases.notifications.NotificationManager;

public class AccountLoginUseCase<T extends Client> extends AccountAuthentication<T> {

    private final String TAG = "AccountLogin";
    public AccountLoginUseCase(AccountRepository<T> accountRepository, NotificationManager notificationManager) {
        super(accountRepository, notificationManager);
    }

    public T execute(AccountValidate<T> validator, String identifier, String password) {
        if (!accountValidateSystem.isValidateAuthenticationData(validator, identifier, password)) {
            Log.e(TAG, "Login failed! Invalid data.");
            return null;
        }

        T client = accountRepository.authentication(validator, identifier, password);

        if (client != null && !accountValidateSystem.isValidateAccountStatus(validator, client)) {
            client.setActiveStatus(true);
            notificationManager.notifyLogin(client.getUsername());
            Log.d(TAG, "Success login!");
            return client;
        }

        if (accountValidateSystem.isValidateAccountStatus(validator, client)){
            Log.e(TAG, "ACCOUNT WAS BANNED!!!!"); // Заглушка чтобы не было ошибки
            return client;
        }

        return null;
    }
}

package ru.mirea.petrakov.ourtour.domain.account.model.TypeClient;

import androidx.annotation.NonNull;

public abstract class Client {
    protected String username;
    protected String password;

    protected Boolean activeStatus = false;
    public Client(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public Boolean getActiveStatus() { return activeStatus; }

    public void setActiveStatus(Boolean activeStatus) { this.activeStatus = activeStatus;}

    @NonNull
    @Override
    public String toString() {
        return "Client{" +
                "username='" + username + '\'' +
                '}';
    }
}

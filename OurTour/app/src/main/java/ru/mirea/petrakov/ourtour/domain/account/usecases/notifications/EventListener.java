package ru.mirea.petrakov.ourtour.domain.account.usecases.notifications;

public interface EventListener {
    void onEvent(String message);
}
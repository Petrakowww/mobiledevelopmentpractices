package ru.mirea.petrakov.ourtour.domain.contacts.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.validators.ReviewValidator;

public class EditReviewUseCase {
    private final static String TAG = "Edit Review";
    private final ReviewValidator reviewValidator;
    private final ReviewRepository reviewRepository;

    public EditReviewUseCase(ReviewValidator reviewValidator, ReviewRepository reviewRepository) {
        this.reviewValidator = reviewValidator;
        this.reviewRepository = reviewRepository;
    }

    public void execute(Review destinationReview) {
        if (reviewValidator.validate(destinationReview)) {
            Review existingReview = reviewRepository.getReviewById(destinationReview.getReviewId());
            if (existingReview != null) {
                reviewRepository.editReview(destinationReview);
                Log.d(TAG, "Review updated successfully: " + destinationReview);
            } else {
                Log.e(TAG, "Cannot edit review, it does not exist.");
            }
        } else {
            Log.e(TAG, "Cannot edit review, not valid");
        }
    }
}
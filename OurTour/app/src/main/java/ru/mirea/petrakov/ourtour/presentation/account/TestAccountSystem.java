package ru.mirea.petrakov.ourtour.presentation.account;

import android.content.Context;
import android.util.Log;

import ru.mirea.petrakov.ourtour.data.account.repository.AccountRepositoryImpl;
import ru.mirea.petrakov.ourtour.domain.account.usecases.valid.AdminValidateImpl;
import ru.mirea.petrakov.ourtour.domain.account.usecases.valid.UserValidateImpl;
import ru.mirea.petrakov.ourtour.domain.account.model.AdminFactoryImpl;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Admin;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;
import ru.mirea.petrakov.ourtour.domain.account.model.UserFactoryImpl;
import ru.mirea.petrakov.ourtour.domain.account.services.UserPoliticImpl;
import ru.mirea.petrakov.ourtour.domain.account.usecases.action.AccountBlockUseCase;
import ru.mirea.petrakov.ourtour.domain.account.usecases.action.AccountChangeUseCase;
import ru.mirea.petrakov.ourtour.domain.account.usecases.action.AccountInfoUseCase;
import ru.mirea.petrakov.ourtour.domain.account.usecases.action.AccountLoginUseCase;
import ru.mirea.petrakov.ourtour.domain.account.usecases.action.AccountLogoutUseCase;
import ru.mirea.petrakov.ourtour.domain.account.usecases.action.AccountRegistrationUseCase;
import ru.mirea.petrakov.ourtour.domain.account.usecases.notifications.EmailNotificationListener;
import ru.mirea.petrakov.ourtour.domain.account.usecases.notifications.NotificationManager;
import ru.mirea.petrakov.ourtour.domain.account.usecases.notifications.PhoneNotificationListener;
import ru.mirea.petrakov.ourtour.presentation.SystemTests;

public class TestAccountSystem implements SystemTests {
    private final Context applicationContext;

    public TestAccountSystem(Context context){
        this.applicationContext = context;
    }
    @Override
    public void RunTestSystem(String logTag) {
        NotificationManager notificationManager = new NotificationManager();

        notificationManager.subscribeToRegistration(new EmailNotificationListener());
        notificationManager.subscribeToLogin(new PhoneNotificationListener());
        notificationManager.subscribeToLogout(new PhoneNotificationListener());

        AccountRepositoryImpl<User> userRepository = new AccountRepositoryImpl<>(
                applicationContext, User.class);

        AccountRegistrationUseCase<User> accountRegistration = new AccountRegistrationUseCase<>(userRepository,
                notificationManager);

        accountRegistration.execute(new UserFactoryImpl(), new UserValidateImpl(),
                "PetrakovEgor",
                "petrakov.egor.g@yandex.ru", "8999999999", "123123123");

        AccountLoginUseCase<User> accountLogin = new AccountLoginUseCase<>(userRepository, notificationManager);

        User user = accountLogin.execute(new UserValidateImpl(), "PetrakovEgor",
                "123123123");

        AccountLogoutUseCase<User> accountLogout = new AccountLogoutUseCase<>(userRepository, notificationManager);

        AccountInfoUseCase accountInfoUseCase = new AccountInfoUseCase(userRepository);
        AccountChangeUseCase accountChangeUseCase = new AccountChangeUseCase(userRepository,
                new UserPoliticImpl());
        AccountBlockUseCase accountBlockUseCase = new AccountBlockUseCase(userRepository,
                new UserPoliticImpl());

        accountInfoUseCase.execute(user);
        accountChangeUseCase.execute(user, "email", "new-email!!!!!");
        accountBlockUseCase.execute(user, false); // - при блокировке сохранит результат при повторном запуске
        accountInfoUseCase.execute(user);

        accountLogout.execute(user);

        // АДМИНИСТРАТОР

        AccountRepositoryImpl<Admin> adminRepository = new AccountRepositoryImpl<>(
                applicationContext, Admin.class);

        AccountRegistrationUseCase<Admin> accountRegistrationAdmin = new AccountRegistrationUseCase<>(adminRepository,
                notificationManager);

        accountRegistrationAdmin.execute(new AdminFactoryImpl(), new AdminValidateImpl(),
                "AdminUser", "", "", "admin123");

        AccountLoginUseCase<Admin> accountLoginAdmin = new AccountLoginUseCase<>(adminRepository, notificationManager);

        Admin admin = accountLoginAdmin.execute(new AdminValidateImpl(), "AdminUser",
                "admin123");

        AccountLogoutUseCase<Admin> accountLogoutAdmin = new AccountLogoutUseCase<>(adminRepository, notificationManager);

        accountLogoutAdmin.execute(admin);

        Log.d(logTag, admin.toString());
    }
}

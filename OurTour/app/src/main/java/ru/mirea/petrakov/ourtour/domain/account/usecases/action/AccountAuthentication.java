package ru.mirea.petrakov.ourtour.domain.account.usecases.action;
import ru.mirea.petrakov.ourtour.domain.account.usecases.valid.AccountValidateSystem;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;
import ru.mirea.petrakov.ourtour.domain.account.usecases.notifications.NotificationManager;

public class AccountAuthentication<T extends Client> {
    protected final AccountRepository<T> accountRepository;
    protected final AccountValidateSystem<T> accountValidateSystem;
    protected final NotificationManager notificationManager;;

    public AccountAuthentication(AccountRepository<T> accountRepository, NotificationManager notificationManager) {
        this.accountRepository = accountRepository;
        this.notificationManager = notificationManager;
        this.accountValidateSystem = new AccountValidateSystem<>();
    }
}

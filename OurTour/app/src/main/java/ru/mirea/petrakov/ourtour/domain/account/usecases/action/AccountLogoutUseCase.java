package ru.mirea.petrakov.ourtour.domain.account.usecases.action;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;
import ru.mirea.petrakov.ourtour.domain.account.usecases.notifications.NotificationManager;

public class AccountLogoutUseCase<T extends Client> extends AccountAuthentication<T> {
    private static final String TAG = "AccountLogout";

    public AccountLogoutUseCase(AccountRepository<T> accountRepository, NotificationManager notificationManager) {
        super(accountRepository, notificationManager);
    }

    public boolean execute(T client) {
        if (client == null) {
            Log.e(TAG, "Logout failed! User is null.");
            return false;
        }

        client.setActiveStatus(false);

        if (accountRepository.logoutInformation(client)) {
            Log.d(TAG, "User logged out successfully.");
            notificationManager.notifyLogout(client.getUsername());
            return true;
        } else {
            Log.e(TAG, "Logout failed! Could not update user status.");
            return false;
        }
    }
}

package ru.mirea.petrakov.ourtour.domain.contacts.model;

import java.util.List;

public class Review {
    private String reviewId;
    private String bookingId;
    private int rating;
    private String comment;
    private List<String> photoUrls;
    private List<String> reply;

    private int helpfulRating;

    public Review(String reviewId, String bookingId, int rating, String comment, List<String> photoUrls) {
        this.reviewId = reviewId;
        this.bookingId = bookingId;
        this.rating = rating;
        this.comment = comment;
        this.photoUrls = photoUrls;
        this.helpfulRating = 0;
    }

    public String getBookingId() {
        return bookingId;
    }

    public int getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public List<String> getReply() {
        return reply;
    }

    public String getReviewId() {
        return reviewId;
    }

    // Setters
    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public void setReply(List<String> reply) {
        this.reply = reply;
    }
    @Override
    public String toString() {
        return "Review{" +
                "reviewId='" + reviewId + '\'' +
                ", bookingId='" + bookingId + '\'' +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                ", photoUrls=" + photoUrls +
                ", reply=" + reply +
                ", helpfulRating=" + helpfulRating +
                '}';
    }


    public int getHelpfulRating() {
        return helpfulRating;
    }

    public void setHelpfulRating(int helpfulRating) {
        this.helpfulRating = helpfulRating;
    }
}

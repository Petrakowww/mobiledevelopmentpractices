package ru.mirea.petrakov.ourtour.domain.orders.usecases;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;
import ru.mirea.petrakov.ourtour.domain.orders.services.BookingAutoCancelService;

public class AddTourToConfirmListUseCase {
    private final static String TAG = "Confirm tour";
    private final BookingRepository bookingRepository;
    private final BookingAutoCancelService bookingAutoCancelService;

    public AddTourToConfirmListUseCase(BookingRepository bookingRepository,
                                       BookingAutoCancelService bookingAutoCancelService) {
        this.bookingRepository = bookingRepository;
        this.bookingAutoCancelService = bookingAutoCancelService;
    }
    public void execute(String bookingId) {
        Booking booking = bookingRepository.getBookingById(bookingId);
        if (booking != null && booking.getStatus() == BookingStatus.PENDING) {
            booking.setStatus(BookingStatus.CONFIRMED);
            bookingRepository.editBooking(booking);

            bookingAutoCancelService.stopAutoCancel();
        }
    }
}

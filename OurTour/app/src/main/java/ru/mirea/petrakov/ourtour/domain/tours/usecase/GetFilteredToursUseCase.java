package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import java.util.List;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class GetFilteredToursUseCase {
    private final TourRepository tourRepository;

    public GetFilteredToursUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public List<Tour> execute(String destination, double minPrice, double maxPrice) {
        return tourRepository.getFilteredTours(destination, minPrice, maxPrice);
    }
}

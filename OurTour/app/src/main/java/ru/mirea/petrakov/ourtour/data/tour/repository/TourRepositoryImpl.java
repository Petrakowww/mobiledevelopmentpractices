package ru.mirea.petrakov.ourtour.data.tour.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class TourRepositoryImpl implements TourRepository {
    private List<Tour> tours;
    private List<Tour> tourHistory;

    public TourRepositoryImpl() {
        tours = new ArrayList<>();
        tourHistory = new ArrayList<>();

        tours.add(new Tour("1",
                "Spain", 1200.0, "A sunny trip to Spain"));
        tours.add(new Tour("2",
                "France", 1500.0, "Romantic Paris tour"));
        tours.add(new Tour("3",
                "Italy", 1300.0, "Explore historical Italy"));
    }

    @Override
    public Tour getTourById(String tourId) {
        // Ищем тур по ID
        for (Tour tour : tours) {
            if (tour.getId().equals(tourId)) {
                tourHistory.add(tour);
                return tour;
            }
        }
        return null;
    }

    @Override
    public List<Tour> getAllTours() {
        return new ArrayList<>(tours);
    }

    @Override
    public List<Tour> getFilteredTours(String destination, double minPrice, double maxPrice) {
        return tours.stream()
                .filter(tour -> tour.getDestination().equalsIgnoreCase(destination)
                        && tour.getPrice() >= minPrice
                        && tour.getPrice() <= maxPrice)
                .collect(Collectors.toList());
    }

    @Override
    public List<Tour> getTourHistory() {
        return new ArrayList<>(tourHistory);
    }

    @Override
    public List<Tour> getFavoriteTours() {
        return tours.stream()
                .filter(Tour::isFavorite)
                .collect(Collectors.toList());
    }
}

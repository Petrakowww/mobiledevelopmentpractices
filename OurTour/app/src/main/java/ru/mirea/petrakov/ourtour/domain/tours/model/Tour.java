package ru.mirea.petrakov.ourtour.domain.tours.model;

public class Tour {
    private String id;
    private String destination;
    private double price;
    private String description;
    private boolean isFavorite;

    public Tour(String id, String destination, double price, String description) {
        this.id = id;
        this.destination = destination;
        this.price = price;
        this.description = description;
        this.isFavorite = false;
    }

    public String getId() {
        return id;
    }

    public String getDestination() {
        return destination;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public String toString() {
        return "Tour {" +
                "ID: '" + id + '\'' +
                ", Destination: '" + destination + '\'' +
                ", Price: $" + price +
                ", Description: '" + description + '\'' +
                ", Is Favorite: " + (isFavorite ? "Yes" : "No") +
                '}';
    }
}

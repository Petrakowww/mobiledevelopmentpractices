package ru.mirea.petrakov.ourtour.domain.account.usecases.valid;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;

public class UserValidateImpl implements AccountValidate<User>{
    @Override
    public boolean validateUsername(String username) {
        return username != null && !username.trim().isEmpty();
    }

    @Override
    public boolean validatePassword(String password) {
        return password != null && password.length() >= 6;
    }

    @Override
    public boolean validateEmail(String email) {
        return email != null && !email.trim().isEmpty();
    }

    @Override
    public boolean validatePhone(String phone) {
        return phone != null && !phone.trim().isEmpty();
    }

    @Override
    public boolean validateClient(User client) {
        return validateUsername(client.getUsername()) &&
                validatePassword(client.getPassword()) &&
                validateEmail(client.getEmail()) &&
                validatePhone(client.getPhoneNumber());
    }

    @Override
    public boolean validateAuth(String identifier, String password) {
        if (identifier.startsWith("+7") || identifier.startsWith("89")) {
            return validatePhone(identifier) && validatePassword(password);
        } else if (identifier.contains("@") && identifier.contains(".")) {
            return validateEmail(identifier) && validatePassword(password);
        } else {
            return validateUsername(identifier) && validatePassword(password);
        }
    }

    @Override
    public boolean validateBlockStatus(User client) {
        return client.isBlock();
    }

    @Override
    public boolean authentication(String identifier, String password, User client) {
        return (client.getUsername().equals(identifier) ||
                client.getEmail().equals(identifier) ||
                client.getPhoneNumber().equals(identifier)) &&
                client.getPassword().equals(password);
    }
}

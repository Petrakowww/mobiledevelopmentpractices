package ru.mirea.petrakov.ourtour.domain.orders.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;

public class CheckBookingStatusUseCase {
    private final BookingRepository bookingRepository;

    public CheckBookingStatusUseCase(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public void execute(String bookingId) {
        Booking booking = bookingRepository.getBookingById(bookingId);

        if (booking != null) {
            Log.d("CheckBookingStatus", "Booking " + bookingId + " status: " + booking.getStatus());
        } else {
            Log.e("CheckBookingStatus", "Booking not found with ID: " + bookingId);
        }
    }
}

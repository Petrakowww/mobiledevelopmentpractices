package ru.mirea.petrakov.ourtour.domain.account.usecases.valid;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;

public class AccountValidateSystem<T extends  Client> {
    public boolean isValidateRegisterData(AccountValidate<T> validator, T client) {
        return validator.validateClient(client);
    }
    public boolean isValidateAuthenticationData(AccountValidate<T> validator,
                                                String identifier, String password) {
        return validator.validateAuth(identifier, password);
    }

    public boolean isValidateAccountStatus(AccountValidate<T> validator, T client){
        return validator.validateBlockStatus(client);
    }
}

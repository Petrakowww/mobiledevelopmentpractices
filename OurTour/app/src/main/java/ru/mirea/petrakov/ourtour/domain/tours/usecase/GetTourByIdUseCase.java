package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class GetTourByIdUseCase {
    private final TourRepository tourRepository;

    public GetTourByIdUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public Tour execute(String tourId) {
        return tourRepository.getTourById(tourId);
    }
}

package ru.mirea.petrakov.ourtour.domain.contacts.usecases.validators;

import java.util.List;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;

public class ReviewValidator {
    private static final int MIN_RATING = 1;
    private static final int MAX_RATING = 5;
    private static final int MAX_PHOTO_COUNT = 5;

    public boolean validate(Review review) {
        return validateRating(review.getRating()) &&
                validateComment(review.getComment()) &&
                validatePhotoCount(review.getPhotoUrls());
    }

    private boolean validateRating(int rating) {
        return rating >= MIN_RATING && rating <= MAX_RATING;
    }

    private boolean validateComment(String comment) {
        return comment != null && !comment.trim().isEmpty();
    }

    private boolean validatePhotoCount(List<String> photoUrls) {
        return photoUrls == null || photoUrls.size() <= MAX_PHOTO_COUNT;
    }
}

package ru.mirea.petrakov.ourtour.domain.contacts.repository;

import java.util.List;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;

public interface ReviewRepository {
    void addReview(Review review);
    Review getReviewById(String reviewId);
    void replyToReview(Review review, String description);
    void increaseReviewRating(Review review);
    void editReview(Review review);
    void deleteReview(Review review);
    List<Review> getAllReviews();
}

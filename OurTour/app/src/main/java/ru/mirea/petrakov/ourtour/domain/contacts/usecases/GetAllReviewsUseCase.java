package ru.mirea.petrakov.ourtour.domain.contacts.usecases;

import java.util.List;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;

public class GetAllReviewsUseCase {
    private final ReviewRepository reviewRepository;

    public GetAllReviewsUseCase(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public List<Review> execute() {
        return reviewRepository.getAllReviews();
    }
}

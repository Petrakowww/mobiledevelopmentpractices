package ru.mirea.petrakov.ourtour.domain.account.usecases.notifications;

import java.util.ArrayList;
import java.util.List;

public class NotificationManager {
    private List<EventListener> loginListeners = new ArrayList<>();
    private List<EventListener> registrationListeners = new ArrayList<>();
    private List<EventListener> logoutListeners = new ArrayList<>();
    public void subscribeToLogin(EventListener listener) {
        loginListeners.add(listener);
    }

    public void subscribeToRegistration(EventListener listener) {
        registrationListeners.add(listener);
    }

    public void subscribeToLogout(EventListener listener) {logoutListeners.add(listener);}

    public void notifyLogin(String username) {
        for (EventListener listener : loginListeners) {
            listener.onEvent("User " + username + " has logged in.");
        }
    }

    public void notifyRegistration(String email) {
        for (EventListener listener : registrationListeners) {
            listener.onEvent("Registration completed for email: " + email);
        }
    }

    public void notifyLogout(String username) {
        for (EventListener listener : loginListeners) {
            listener.onEvent("User " + username + " has logout.");
        }
    }
}
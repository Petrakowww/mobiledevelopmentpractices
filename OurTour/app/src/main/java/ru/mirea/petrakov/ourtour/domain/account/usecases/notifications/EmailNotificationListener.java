package ru.mirea.petrakov.ourtour.domain.account.usecases.notifications;

import android.util.Log;

public class EmailNotificationListener implements EventListener {
    private static final String TAG = "EmailListener";
    @Override
    public void onEvent(String message) {
        Log.d(TAG, "Email Notification: " + message);
    }
}

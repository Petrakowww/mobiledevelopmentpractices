package ru.mirea.petrakov.ourtour.domain.account.usecases.action;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;
import ru.mirea.petrakov.ourtour.domain.account.services.ClientPolitic;

public abstract class AccountAction {
    protected final String TAG = "ACTION ON USER";
    protected final AccountRepository<User> accountRepository;
    protected final ClientPolitic<User> clientPolitic;

    public AccountAction(AccountRepository<User> accountRepository, ClientPolitic<User> clientPolitic) {
        this.accountRepository = accountRepository;
        this.clientPolitic = clientPolitic;
    }
}

package ru.mirea.petrakov.ourtour.domain.account.repository;

import ru.mirea.petrakov.ourtour.domain.account.usecases.valid.AccountValidate;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;

public interface AccountRepository<T extends Client> {
    public T authentication(AccountValidate<T> accountValidate, String identifier, String password);
    public boolean registrationInformation(T client);
    public boolean logoutInformation(T client);
    public String getUserInformation(T client);
    public void changeUserInformation(T client);
}

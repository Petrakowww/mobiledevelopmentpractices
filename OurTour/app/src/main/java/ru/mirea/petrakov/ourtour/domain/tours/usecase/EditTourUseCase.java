package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class EditTourUseCase {
    private final TourRepository tourRepository;

    public EditTourUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public void execute(Tour updatedTour) {
        Tour existingTour = tourRepository.getTourById(updatedTour.getId());
        if (existingTour != null) {
            existingTour.setFavorite(updatedTour.isFavorite());
        }
    }
}

package ru.mirea.petrakov.ourtour.domain.account.services;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;

public interface ClientPolitic<T extends Client> {
    public void politicBanned(T user, boolean flag);
    public void politicChangedData(T user, String key, String value);
}

package ru.mirea.petrakov.ourtour.data.account.repository;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import ru.mirea.petrakov.ourtour.domain.account.usecases.valid.AccountValidate;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Admin;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;

public class AccountRepositoryImpl<T extends Client> implements AccountRepository<T> {
    private static final String PREF_NAME = "user_prefs";
    private static final String USERS_KEY = "users";
    private static final String ADMINS_KEY = "admins";
    private final SharedPreferences sharedPreferences;
    private final Gson gson;

    private final Class<T> type;

    public AccountRepositoryImpl(Context context, Class<T> type) {
        this.type = type;
        this.sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.gson = new Gson();
    }

    @Override
    public boolean registrationInformation(T client) {
        Map<String, T> clients = getClientsFromPreferences();

        if (clients.containsKey(client.getUsername())) {
            return false;
        }

        client.setActiveStatus(false);
        clients.put(client.getUsername(), client);

        saveClientsToPreferences(clients);

        return true;
    }

    @Override
    public boolean logoutInformation(T client) {
        Map<String, T> clients = getClientsFromPreferences();

        if (clients.containsKey(client.getUsername())) {
            client.setActiveStatus(false);
            clients.put(client.getUsername(), client);
            saveClientsToPreferences(clients);
            return true;
        }

        return false;
    }

    @Override
    public String getUserInformation(T client) {
        Map<String, T> clients = getClientsFromPreferences();
        Client clientRep = clients.get(client.getUsername());
        return clientRep != null ? clientRep.toString() : "No information";
    }

    @Override
    public void changeUserInformation(T client) {
        Map<String, T> clients = getClientsFromPreferences();

        clients.put(client.getUsername(), client);
        saveClientsToPreferences(clients);
    }

    @Override
    public T authentication(AccountValidate<T> authenticationValidator,
                            String identifier, String password) {
        Map<String, T> clients = getClientsFromPreferences();

        for (T client : clients.values()) {
            if (authenticationValidator.authentication(identifier, password, client)) {
                return client;
            }
        }

        return null;
    }

    private Map<String, T> getClientsFromPreferences() {
        String json = sharedPreferences.getString(getKeyForType(), "");
        if (json.isEmpty()) {
            return new HashMap<>();
        }

        Type type = TypeToken.getParameterized(Map.class, String.class, this.type).getType();
        return gson.fromJson(json, type);
    }

    private void saveClientsToPreferences(Map<String, T> clients) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = gson.toJson(clients);
        editor.putString(getKeyForType(), json);
        editor.apply();
    }

    private String getKeyForType() {
        if (type.equals(User.class)) {
            return USERS_KEY;
        } else if (type.equals(Admin.class)) {
            return ADMINS_KEY;
        } else {
            throw new IllegalArgumentException("Unsupported client type");
        }
    }
}

package ru.mirea.petrakov.ourtour.domain.account.usecases.valid;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Admin;

public class AdminValidateImpl implements AccountValidate<Admin>{
    @Override
    public boolean validateUsername(String username) {
        return true;
    }

    @Override
    public boolean validatePassword(String password) {
        return true;
    }

    @Override
    public boolean validateEmail(String email) {
        return true;
    }

    @Override
    public boolean validatePhone(String phone) {
        return true;
    }

    @Override
    public boolean validateClient(Admin client) {
        return validateUsername(client.getUsername()) && validatePassword(client.getPassword());
    }

    @Override
    public boolean validateAuth(String identifier, String password) {
        return validateUsername(identifier) && validatePassword(password);
    }

    @Override
    public boolean validateBlockStatus(Admin client) {
        return false;
    }

    @Override
    public boolean authentication(String identifier, String password, Admin client) {
        return client.getUsername().equals(identifier) &&
                client.getPassword().equals(password);
    }
}

package ru.mirea.petrakov.ourtour.domain.contacts.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;
import ru.mirea.petrakov.ourtour.domain.contacts.usecases.validators.ReviewValidator;

public class AddReviewUseCase {
    private final static String TAG = "Add Review";
    private final ReviewValidator reviewValidator;
    private final ReviewRepository reviewRepository;

    public AddReviewUseCase(ReviewValidator reviewValidator, ReviewRepository reviewRepository) {
        this.reviewValidator = reviewValidator;
        this.reviewRepository = reviewRepository;
    }

    public void execute(Review review) {
        if (reviewValidator.validate(review)){
            reviewRepository.addReview(review);
        }
        else {
            Log.e(TAG, "Cannot add review, not valid");
        }
    }
}

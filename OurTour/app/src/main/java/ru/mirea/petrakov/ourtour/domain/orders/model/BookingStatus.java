package ru.mirea.petrakov.ourtour.domain.orders.model;

public enum BookingStatus {
    PENDING,
    CONFIRMED,
    CANCELED
}

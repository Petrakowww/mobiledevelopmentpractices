package ru.mirea.petrakov.ourtour.domain.orders.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;

public class ChangeBookingStatusUseCase {
    private final static String TAG = "Change Booking Status";
    private final BookingRepository bookingRepository;

    public ChangeBookingStatusUseCase(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public void changeStatus(String bookingId, BookingStatus newStatus) {
        if (bookingId == null || newStatus == null) {
            Log.e(TAG, "Booking ID and status must not be null.");
        }
        bookingRepository.changeBookingStatus(bookingId, newStatus);
    }
}

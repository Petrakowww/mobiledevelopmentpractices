package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class RemoveTourFromFavoritesUseCase {
    private final TourRepository tourRepository;

    public RemoveTourFromFavoritesUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }
    public void execute(Tour tour) {
        tour.setFavorite(false);
        Log.d("Favorites", "Tour removed from favorites: " + tour);
    }
}

package ru.mirea.petrakov.ourtour.presentation.tour;

import android.util.Log;

import java.util.List;

import ru.mirea.petrakov.ourtour.data.tour.repository.TourRepositoryImpl;
import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.AddTourUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.DeleteTourUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.EditTourUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.GetAllToursUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.GetFavoriteTourUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.GetFilteredToursUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.GetInfoTourUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.GetTourByIdUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.GetTourHistoryUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.RemoveTourFromFavoritesUseCase;
import ru.mirea.petrakov.ourtour.domain.tours.usecase.AddTourToFavoritesUseCase;
import ru.mirea.petrakov.ourtour.presentation.SystemTests;

public class TestTourSystem implements SystemTests {
    @Override
    public void RunTestSystem(String logTag) {
        TourRepositoryImpl tourRepository = new TourRepositoryImpl();

        GetAllToursUseCase getAllToursUseCase = new GetAllToursUseCase(tourRepository);
        GetTourByIdUseCase getTourByIdUseCase = new GetTourByIdUseCase(tourRepository);
        GetFilteredToursUseCase getFilteredToursUseCase = new GetFilteredToursUseCase(tourRepository);
        GetTourHistoryUseCase getTourHistoryUseCase = new GetTourHistoryUseCase(tourRepository);
        GetInfoTourUseCase getInfoTourUseCase = new GetInfoTourUseCase(tourRepository);
        GetFavoriteTourUseCase getFavoriteTourUseCase = new GetFavoriteTourUseCase(tourRepository);
        AddTourUseCase addTourUseCase = new AddTourUseCase(tourRepository);
        EditTourUseCase editTourUseCase = new EditTourUseCase(tourRepository);
        DeleteTourUseCase deleteTourUseCase = new DeleteTourUseCase(tourRepository);
        AddTourToFavoritesUseCase addTourToFavoritesUseCase = new AddTourToFavoritesUseCase(tourRepository);
        RemoveTourFromFavoritesUseCase removeTourFromFavoritesUseCase = new RemoveTourFromFavoritesUseCase(tourRepository);

        List<Tour> allTours = getAllToursUseCase.execute();
        for (Tour tour : allTours) {
            Log.d(logTag, tour.toString());
        }

        Tour tour = getTourByIdUseCase.execute("1");
        Log.d(logTag, "\nTour by ID: " + tour);

        List<Tour> filteredTours = getFilteredToursUseCase.execute("Spain", 1000.0, 1300.0);
        Log.d(logTag, "\nFiltered Tours:");
        for (Tour filteredTour : filteredTours) {
            Log.d(logTag, filteredTour.toString());
        }

        List<Tour> tourHistory = getTourHistoryUseCase.execute();
        Log.d(logTag, "\nTour History:");
        for (Tour historyTour : tourHistory) {
            Log.d(logTag, historyTour.toString());
        }

        Log.d(logTag, "\nTour info:" + getInfoTourUseCase.execute("1"));

        List<Tour> favoriteTours = getFavoriteTourUseCase.execute();
        Log.d(logTag, "\nFavorite Tours:");
        for (Tour favoriteTour : favoriteTours) {
            Log.d(logTag, favoriteTour.toString());
        }

        Tour newTour = new Tour("5", "France", 1200.0,
                "A wonderful tour of France");
        addTourUseCase.execute(newTour);
        Log.d(logTag, "\nAdded Tour: " + newTour.toString());

        newTour.setFavorite(true);  // Пример изменения поля
        editTourUseCase.execute(newTour);
        Log.d(logTag, "\nEdited Tour: " + newTour.toString());

        deleteTourUseCase.execute("5");
        Log.d(logTag, "\nDeleted Tour with ID 5");

        Tour tourTest = new Tour("10", "Moscow", 1000,
                "All Moscow - One Day");

        addTourToFavoritesUseCase.execute(tourTest);
        Log.d(logTag, "\nAdded Tour ID 1 to Favorites");

        removeTourFromFavoritesUseCase.execute(tourTest);
        Log.d(logTag, "\nRemoved Tour ID 1 from Favorites");
    }
}

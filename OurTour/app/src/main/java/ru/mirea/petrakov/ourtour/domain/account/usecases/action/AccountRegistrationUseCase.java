package ru.mirea.petrakov.ourtour.domain.account.usecases.action;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.account.usecases.valid.AccountValidate;
import ru.mirea.petrakov.ourtour.domain.account.model.ClientFactory;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;
import ru.mirea.petrakov.ourtour.domain.account.usecases.notifications.NotificationManager;

public class AccountRegistrationUseCase<T extends Client> extends AccountAuthentication<T> {
    private static final String TAG = "AccountRegistration";

    public AccountRegistrationUseCase(AccountRepository<T> accountRepository,
                                      NotificationManager notificationManager) {
        super(accountRepository, notificationManager);
    }

    public T execute(ClientFactory<T> factoryClient, AccountValidate<T> validator,
                     String username, String email, String phone, String password) {
        T client = factoryClient.createClient(username, password, email, phone);
        if (!accountValidateSystem.isValidateRegisterData(validator, client)) {
            Log.e(TAG, "Registration failed! Invalid data.");
            return null;
        }
        if (accountRepository.registrationInformation(client)) {
            Log.d(TAG, "User registered successfully.");
            notificationManager.notifyRegistration(email);
            return client;
        } else {
            Log.e(TAG, "Registration failed! Repository error.");
        }
        return null;
    }
}

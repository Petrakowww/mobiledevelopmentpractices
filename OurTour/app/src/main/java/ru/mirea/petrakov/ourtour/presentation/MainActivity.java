package ru.mirea.petrakov.ourtour.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ru.mirea.petrakov.ourtour.R;
import ru.mirea.petrakov.ourtour.presentation.account.TestAccountSystem;
import ru.mirea.petrakov.ourtour.presentation.booking.TestBookingSystem;
import ru.mirea.petrakov.ourtour.presentation.contacts.TestContactSystem;
import ru.mirea.petrakov.ourtour.presentation.tour.TestTourSystem;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SystemTests systemTestsAccount = new TestAccountSystem(getApplicationContext());

        systemTestsAccount.RunTestSystem(TAG + " ACCOUNT SYSTEM");

        SystemTests systemTestsTour = new TestTourSystem();

        systemTestsTour.RunTestSystem(TAG + " TOUR SYSTEM");

        SystemTests systemTestsBooking = new TestBookingSystem();

        systemTestsBooking.RunTestSystem(TAG + " BOOKING SYSTEM");

        SystemTests systemTestsContact = new TestContactSystem();

        systemTestsContact.RunTestSystem(TAG + " CONTACT SYSTEM");
    }
}
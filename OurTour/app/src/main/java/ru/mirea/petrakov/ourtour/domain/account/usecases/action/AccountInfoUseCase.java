package ru.mirea.petrakov.ourtour.domain.account.usecases.action;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.Client;
import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;
import ru.mirea.petrakov.ourtour.domain.account.repository.AccountRepository;

public class AccountInfoUseCase {
    private final AccountRepository<User> accountRepository;

    public AccountInfoUseCase(AccountRepository<User> accountRepository) {
        this.accountRepository = accountRepository;
    }

    public String execute(User client){
        return accountRepository.getUserInformation(client);
    }
}

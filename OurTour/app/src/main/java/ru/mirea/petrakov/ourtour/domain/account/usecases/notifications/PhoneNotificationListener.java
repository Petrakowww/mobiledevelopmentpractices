package ru.mirea.petrakov.ourtour.domain.account.usecases.notifications;

import android.util.Log;

public class PhoneNotificationListener implements EventListener {
    private static final String TAG = "PhoneListener";
    @Override
    public void onEvent(String message) {
        Log.d(TAG,"Phone Notification: " + message);
    }
}
package ru.mirea.petrakov.ourtour.data.booking.repository;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;

public class BookingRepositoryImpl implements BookingRepository {
    private final List<Booking> bookings = new ArrayList<>();

    @Override
    public void addBooking(Booking booking) {
        bookings.add(booking);
    }

    @Override
    public void deleteBooking(String bookingId) {
        bookings.removeIf(booking -> booking.getBookingId().equals(bookingId));
    }

    @Override
    public void editBooking(Booking booking) {
        Optional<Booking> existingBookingOpt = bookings.stream()
                .filter(b -> b.getBookingId().equals(booking.getBookingId()))
                .findFirst();

        if (existingBookingOpt.isPresent()) {
            Booking existingBooking = existingBookingOpt.get();
            existingBooking.setStatus(booking.getStatus());
            existingBooking.setBookedSeats(booking.getBookedSeats());
            existingBooking.setAvailableSeats(booking.getAvailableSeats());
        }
    }

    @Override
    public Booking getBookingById(String bookingId) {
        return bookings.stream()
                .filter(booking -> booking.getBookingId().equals(bookingId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<Booking> getAllBookings() {
        return new ArrayList<>(bookings);
    }

    @Override
    public void changeBookingStatus(String bookingId, BookingStatus newStatus) {
        Booking booking = getBookingById(bookingId);
        if (booking != null) {
            booking.setStatus(newStatus);
            Log.d("BookingRepository", "Booking " + bookingId + " status changed to " + newStatus);
        } else {
            Log.e("BookingRepository", "Booking not found with ID: " + bookingId);
        }
    }
}

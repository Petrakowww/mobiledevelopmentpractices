package ru.mirea.petrakov.ourtour.domain.contacts.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;

public class DeleteReviewByIdUseCase {
    private final static String TAG = "Delete Review";
    private final ReviewRepository reviewRepository;

    public DeleteReviewByIdUseCase(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public void execute(String reviewId) {
        Review reviewToDelete = reviewRepository.getReviewById(reviewId);
        if (reviewToDelete != null) {
            reviewRepository.deleteReview(reviewToDelete);
        } else {
            Log.e(TAG, "Cannot delete review, not exist valid");
            System.err.println("Review with ID " + reviewId + " not found, cannot delete.");
        }
    }
}

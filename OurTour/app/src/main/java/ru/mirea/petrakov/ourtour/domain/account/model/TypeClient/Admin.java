package ru.mirea.petrakov.ourtour.domain.account.model.TypeClient;

import androidx.annotation.NonNull;

public class Admin extends Client {

    public Admin(String username, String password) {
        super(username, password);
    }

    @NonNull
    @Override
    public String toString() {
        return "Admin{" +
                "username='" + username + '\'' +
                '}';
    }
}

package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class AddTourToFavoritesUseCase {
    private final TourRepository tourRepository;

    public AddTourToFavoritesUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public void execute(Tour tour) {
        tour.setFavorite(true);
        Log.d("Favorites", "Tour added to favorites: " + tour);
    }
}

package ru.mirea.petrakov.ourtour.presentation.booking;

import android.os.Handler;
import android.util.Log;

import java.util.List;

import ru.mirea.petrakov.ourtour.data.booking.repository.BookingRepositoryImpl;
import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;
import ru.mirea.petrakov.ourtour.domain.orders.services.BookingAutoCancelService;
import ru.mirea.petrakov.ourtour.domain.orders.usecases.AddTourToBookingUseCase;
import ru.mirea.petrakov.ourtour.domain.orders.usecases.AddTourToConfirmListUseCase;
import ru.mirea.petrakov.ourtour.domain.orders.usecases.ChangeBookingStatusUseCase;
import ru.mirea.petrakov.ourtour.domain.orders.usecases.CheckBookingStatusUseCase;
import ru.mirea.petrakov.ourtour.domain.orders.usecases.DeleteBookingUseCase;
import ru.mirea.petrakov.ourtour.domain.orders.usecases.EditBookingUseCase;
import ru.mirea.petrakov.ourtour.domain.orders.usecases.ViewBookingUseCase;
import ru.mirea.petrakov.ourtour.presentation.SystemTests;

public class TestBookingSystem implements SystemTests {

    @Override
    public void RunTestSystem(String logTag) {
        BookingRepository bookingRepository = new BookingRepositoryImpl();
        BookingAutoCancelService bookingAutoCancelService = new BookingAutoCancelService(bookingRepository);

        AddTourToBookingUseCase addTourToBookingUseCase = new AddTourToBookingUseCase(bookingRepository, bookingAutoCancelService);
        AddTourToConfirmListUseCase confirmBookingUseCase = new AddTourToConfirmListUseCase(bookingRepository, bookingAutoCancelService);
        ViewBookingUseCase viewBookingUseCase = new ViewBookingUseCase(bookingRepository);
        EditBookingUseCase editBookingUseCase = new EditBookingUseCase(bookingRepository, bookingAutoCancelService);
        DeleteBookingUseCase deleteBookingUseCase = new DeleteBookingUseCase(bookingRepository, bookingAutoCancelService);
        CheckBookingStatusUseCase checkBookingStatusUseCase = new CheckBookingStatusUseCase(bookingRepository);
        ChangeBookingStatusUseCase changeBookingStatusUseCase = new ChangeBookingStatusUseCase(bookingRepository);


        // Добавление первого бронирования, которое не подтверждается
        addTourToBookingUseCase.execute("booking1", "tour1", "user1", 2, 5, 10); // Таймаут 10 секунд
        Log.d(logTag, "Added booking1, waiting for auto-cancel in 10 seconds.");

        // Добавление второго бронирования, которое будет подтверждено через 5 секунд
        addTourToBookingUseCase.execute("booking2", "tour1", "user2", 1, 5, 10); // Таймаут 10 секунд
        Log.d(logTag, "Added booking2, will confirm in 5 seconds.");

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                confirmBookingUseCase.execute("booking2");
                Log.d(logTag, "Booking2 confirmed after 5 seconds.");
            }
        }, 5000); // 5 секунд задержки

        // Проверка бронирования через 6 секунд
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkBookingStatusUseCase.execute("booking2");
                Log.d(logTag, "Checked status for booking2 after 6 seconds.");
            }
        }, 6000);

        // Редактирование первого бронирования через 7 секунд
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Booking booking1 = bookingRepository.getBookingById("booking1");
                booking1.setSeatsToBook(3);
                editBookingUseCase.execute(booking1, 10); // Перезапуск таймера на 10 секунд
                Log.d(logTag, "Edited booking1 after 7 seconds.");
                viewBookingUseCase.execute(booking1.getBookingId());
            }
        }, 7000);

        // Удаляем первое бронирование через 8 секунд
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                deleteBookingUseCase.execute("booking1");
                Log.d(logTag, "Deleted booking1 after 8 seconds.");
            }
        }, 8000);

        // Проверяем все бронирования через 14 секунд
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                List<Booking> allBookings = bookingRepository.getAllBookings();
                Log.d(logTag, "Bookings after 14 seconds: " + allBookings);
            }
        }, 14000); // Задержка 14 секунд

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                changeBookingStatusUseCase.changeStatus("booking2",
                        BookingStatus.CANCELED);
                viewBookingUseCase.execute("booking2");
                List<Booking> allBookings = bookingRepository.getAllBookings();
                Log.d(logTag, "Bookings after 16 seconds: " + allBookings);
            }
        }, 16000); // Задержка 16 секунд

    }
}

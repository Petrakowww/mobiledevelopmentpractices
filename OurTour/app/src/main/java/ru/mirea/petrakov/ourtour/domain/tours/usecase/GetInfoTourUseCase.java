package ru.mirea.petrakov.ourtour.domain.tours.usecase;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;
import ru.mirea.petrakov.ourtour.domain.tours.repository.TourRepository;

public class GetInfoTourUseCase {
    protected final TourRepository tourRepository;

    public GetInfoTourUseCase(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }
    public String execute(String tourId) {
        return tourRepository.getTourById(tourId).toString();
    }
}

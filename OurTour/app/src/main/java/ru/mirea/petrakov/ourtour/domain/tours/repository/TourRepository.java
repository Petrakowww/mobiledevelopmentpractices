package ru.mirea.petrakov.ourtour.domain.tours.repository;

import java.util.List;

import ru.mirea.petrakov.ourtour.domain.tours.model.Tour;

public interface TourRepository {
    Tour getTourById(String tourId);
    List<Tour> getAllTours();
    List<Tour> getFilteredTours(String destination, double minPrice, double maxPrice);
    List<Tour> getTourHistory();
    List<Tour> getFavoriteTours();
}

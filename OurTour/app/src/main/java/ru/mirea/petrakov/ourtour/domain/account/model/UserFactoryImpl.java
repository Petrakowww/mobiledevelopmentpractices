package ru.mirea.petrakov.ourtour.domain.account.model;

import ru.mirea.petrakov.ourtour.domain.account.model.TypeClient.User;

public class UserFactoryImpl implements ClientFactory<User> {
    @Override
    public User createClient(String username, String password, String email, String phone) {
        return new User(username, password, email, phone);
    }
}

package ru.mirea.petrakov.ourtour.domain.orders.repository;

import java.util.List;

import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;

public interface BookingRepository {
    void addBooking(Booking booking);
    void deleteBooking(String bookingId);
    void editBooking(Booking booking);
    Booking getBookingById(String bookingId);
    List<Booking> getAllBookings();
    void changeBookingStatus(String bookingId, BookingStatus newStatus);
}

package ru.mirea.petrakov.ourtour.domain.contacts.usecases;

import android.util.Log;

import ru.mirea.petrakov.ourtour.domain.contacts.model.Review;
import ru.mirea.petrakov.ourtour.domain.contacts.repository.ReviewRepository;

public class ReplyReviewUseCase {
    private final static String TAG = "Reply Review";
    private final ReviewRepository reviewRepository;

    public ReplyReviewUseCase(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public void execute(Review destinationReview, String reply) {
        if (destinationReview != null){
            reviewRepository.replyToReview(destinationReview, reply);
        }
        Log.e(TAG, "Review not founded");
    }
}

package ru.mirea.petrakov.ourtour.domain.orders.services;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import android.util.Log;


import ru.mirea.petrakov.ourtour.domain.orders.model.Booking;
import ru.mirea.petrakov.ourtour.domain.orders.model.BookingStatus;
import ru.mirea.petrakov.ourtour.domain.orders.repository.BookingRepository;

public class BookingAutoCancelService {
    private final BookingRepository bookingRepository;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private static final String TAG = "AutoCancelService";
    private ScheduledFuture<?> cancelFuture;

    public BookingAutoCancelService(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public void scheduleAutoCancel(Booking booking, long delayInSeconds) {
        Runnable cancelTask = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Checking booking " + booking.getBookingId() + " for auto-cancel...");
                Booking currentBooking = bookingRepository.getBookingById(booking.getBookingId());
                if (currentBooking != null && currentBooking.getStatus() == BookingStatus.PENDING) {
                    Log.d(TAG, "Booking " + currentBooking.getBookingId() + " is being canceled due to timeout.");
                    bookingRepository.deleteBooking(currentBooking.getBookingId());
                } else {
                    Log.d(TAG, "Booking " + booking.getBookingId() + " is not pending, skipping auto-cancel.");
                }
                bookingRepository.deleteBooking(booking.getBookingId());
            }
        };

        cancelFuture = scheduler.schedule(cancelTask, delayInSeconds, TimeUnit.SECONDS);
    }

    public void stopAutoCancel() {
        if (cancelFuture != null && !cancelFuture.isDone()) {
            Log.d(TAG, "Cancelling auto-cancel task.");
            cancelFuture.cancel(false);
        }
    }
}

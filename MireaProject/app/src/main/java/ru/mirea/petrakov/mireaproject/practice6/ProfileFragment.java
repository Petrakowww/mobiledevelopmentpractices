package ru.mirea.petrakov.mireaproject.practice6;


import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.mirea.petrakov.mireaproject.databinding.FragmentProfileBinding;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private EditText editTextProfileName;
    private EditText editTextWorkPlace;
    private EditText editTextAddress;
    private EditText editTextFIO;
    private EditText editTextEmail;
    private EditText editTextPhone;
    private EditText editTextGit;
    private EditText editTextMultiLineDesc;

    private Button saveButton;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Init();
        saveButton.setOnClickListener(v -> saveDataButton());
        loadSavedData();
    }

    private void Init(){
        editTextProfileName = binding.editTextProfileName;
        editTextWorkPlace = binding.editTextWorkPlace;
        editTextAddress = binding.editTextAddress;
        editTextFIO = binding.editTextFIO;
        editTextEmail = binding.editTextEmail;
        editTextPhone = binding.editTextPhone;
        editTextGit = binding.editTextGit;
        editTextMultiLineDesc = binding.editTextMultiLineDesc;
        saveButton = binding.saveButton;
    }

    private void saveDataButton() {

        String profileName = editTextProfileName.getText().toString();
        String workPlace = editTextWorkPlace.getText().toString();
        String address = editTextAddress.getText().toString();
        String fio = editTextFIO.getText().toString();
        String email = editTextEmail.getText().toString();
        String phone = editTextPhone.getText().toString();
        String git = editTextGit.getText().toString();
        String desc = editTextMultiLineDesc.getText().toString();
        SharedPreferences sharedPref = requireContext().getSharedPreferences("profile_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("PROFILE_NAME", profileName);
        editor.putString("WORK_PLACE", workPlace);
        editor.putString("ADDRESS", address);
        editor.putString("FIO", fio);
        editor.putString("EMAIL", email);
        editor.putString("PHONE", phone);
        editor.putString("GIT", git);
        editor.putString("DESCRIPTION", desc);
        editor.apply();
    }

    private void loadSavedData() {
        SharedPreferences sharedPref = requireContext().getSharedPreferences("profile_data", Context.MODE_PRIVATE);
        editTextProfileName.setText(sharedPref.getString("PROFILE_NAME", ""));
        editTextWorkPlace.setText(sharedPref.getString("WORK_PLACE", ""));
        editTextAddress.setText(sharedPref.getString("ADDRESS", ""));
        editTextFIO.setText(sharedPref.getString("FIO", ""));
        editTextEmail.setText(sharedPref.getString("EMAIL", ""));
        editTextPhone.setText(sharedPref.getString("PHONE", ""));
        editTextGit.setText(sharedPref.getString("GIT", ""));
        editTextMultiLineDesc.setText(sharedPref.getString("DESCRIPTION", ""));
    }
}
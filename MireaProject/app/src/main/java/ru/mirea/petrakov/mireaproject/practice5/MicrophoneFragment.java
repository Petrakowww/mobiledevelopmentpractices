package ru.mirea.petrakov.mireaproject.practice5;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import ru.mirea.petrakov.mireaproject.databinding.FragmentMicrophoneBinding;

public class MicrophoneFragment extends Fragment {
    private FragmentMicrophoneBinding binding;
    private Button startButton;
    private TextView resultTextView;

    private boolean isListening = false;
    private double maxDecibels = 0.0;
    private AudioRecord audioRecord;

    int bufferSize = AudioRecord.getMinBufferSize(44100,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_PCM_16BIT);

    public MicrophoneFragment() { }

    public static MicrophoneFragment newInstance() {
        return new MicrophoneFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentMicrophoneBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        startButton = binding.startButton;
        resultTextView = binding.textViewResult;

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isListening) {
                    startListening();
                } else {
                    stopListening();
                }
            }
        });

        return view;
    }

    private void requestAudioPermission() {
        int RECORD_AUDIO_PERMISSION_REQUEST_CODE = 200;
        ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.RECORD_AUDIO}, RECORD_AUDIO_PERMISSION_REQUEST_CODE);
    }

    @SuppressLint("MissingPermission")
    private void initializeAudioRecording() {

        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            return;
        }

        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                44100,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize);
    }

    private void startRecording() {
        audioRecord.startRecording();
        isListening = true;
        startButton.setText("Stop Listening");
    }

    private void evaluateSoundIntensity() {
        short[] buffer = new short[bufferSize];
        while (isListening) {
            audioRecord.read(buffer, 0, bufferSize);
            double maxAmplitude = 0;
            for (short s : buffer) {
                maxAmplitude = Math.max(maxAmplitude, Math.abs(s));
            }
            double normalizedAmplitude = maxAmplitude / Short.MAX_VALUE;
            double linearAmplitude = Math.exp(normalizedAmplitude);
            double decibels = 20 * Math.log10(linearAmplitude);
            Log.d("INFO_THREAD", "Normalized Amplitude: " + normalizedAmplitude);
            Log.d("INFO_THREAD", "Decibels: " + decibels);
            if (decibels > maxDecibels) {
                maxDecibels = decibels;
                getActivity().runOnUiThread(() -> resultTextView.setText(String.format(Locale.getDefault(), "%.2f", maxDecibels)));
            }
        }
        stopListening();
    }


    private void stopRecording() {
        isListening = false;
        startButton.setText("Start Listening");

        if (audioRecord != null) {
            audioRecord.stop();
            audioRecord.release();
        }
    }

    private void startListening() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            requestAudioPermission();
            return;
        }

        initializeAudioRecording();

        if (audioRecord != null) {
            startRecording();
            new Thread(this::evaluateSoundIntensity).start();
        }
    }

    private void stopListening() {
        if (isListening) {
            stopRecording();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopListening();
    }
}

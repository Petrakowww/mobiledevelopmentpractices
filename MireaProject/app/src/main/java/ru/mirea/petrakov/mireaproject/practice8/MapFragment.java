package ru.mirea.petrakov.mireaproject.practice8;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import ru.mirea.petrakov.mireaproject.databinding.FragmentMapBinding;

public class MapFragment extends Fragment implements MapEventsReceiver {

    private FragmentMapBinding binding;
    private MapView mapView;
    private MyLocationNewOverlay locationNewOverlay;

    private Spinner spinnerPointA;
    private Spinner spinnerPointB;
    private Button btnSetRoute;
    private DistancePointerDrawer routeManager;
    private SpinnerMapSetter spinnerMapLogic;
    private final String TAG = this.getClass().getSimpleName();

    private final ActivityResultLauncher<String> requestPermissionLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    loadUserLocation();
                } else {
                    Toast.makeText(getContext(), "Permission denied", Toast.LENGTH_LONG).show();
                }
            });

    public MapFragment() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMapBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();
        Configuration.getInstance().load(getContext(),
                PreferenceManager.getDefaultSharedPreferences(getContext()));

        initActions();

        mapView.getOverlayManager().add(new MapEventsOverlay(this));

        setUserLocation();

        addCompass();
        displayingMetricScale();


        return rootView;
    }

    private void initActions(){
        mapView = binding.mapView;
        spinnerPointA = binding.spinnerPointA;
        spinnerPointB = binding.spinnerPointB;
        btnSetRoute = binding.btnSetRoute;

        routeManager = new DistancePointerDrawer(getContext(), mapView);
        spinnerMapLogic = new SpinnerMapSetter(getContext(), spinnerPointA,
                spinnerPointB);

        btnSetRoute.setOnClickListener(v -> {
            GeoPoint [] geoPoints = spinnerMapLogic.tryGetPoints();
            if (geoPoints != null){
                routeManager.startDrawing(geoPoints);
            }
        });
    }
    private void setUserLocation() {
        int locationPermissionStatus = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (locationPermissionStatus == PackageManager.PERMISSION_GRANTED) {
            loadUserLocation();
        } else {
            requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    public void loadUserLocation() {
        locationNewOverlay = new MyLocationNewOverlay(
                new GpsMyLocationProvider(getContext()), mapView);
        locationNewOverlay.enableMyLocation();
        locationNewOverlay.enableFollowLocation();
        mapView.getOverlays().add(this.locationNewOverlay);

        mapView.setZoomRounding(true);
        mapView.setMultiTouchControls(true);

        IMapController mapController = mapView.getController();
        mapController.setZoom(15.0);
    }

    @Override
    public void onResume() {
        super.onResume();
        Configuration.getInstance().load(getContext(),
                PreferenceManager.getDefaultSharedPreferences(getContext()));
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Configuration.getInstance().save(getContext(),
                PreferenceManager.getDefaultSharedPreferences(getContext()));
        if (mapView != null) {
            mapView.onPause();
        }
    }

    private void addCompass() {
        CompassOverlay compassOverlay = new CompassOverlay(getContext(), new
                InternalCompassOrientationProvider(getContext()), mapView);
        compassOverlay.enableCompass();
        mapView.getOverlays().add(compassOverlay);
    }

    private void displayingMetricScale() {
        final Context context = this.getContext();
        final DisplayMetrics dm = context.getResources().getDisplayMetrics();
        ScaleBarOverlay scaleBarOverlay = new ScaleBarOverlay(mapView);
        scaleBarOverlay.setCentred(true);
        scaleBarOverlay.setScaleBarOffset(dm.widthPixels / 2, 10);
        mapView.getOverlays().add(scaleBarOverlay);
    }


    @Override
    public boolean singleTapConfirmedHelper(GeoPoint p) {
        showAddPointDialog(p.getLatitude(), p.getLongitude());
        return true;
    }

    private void showAddPointDialog(double latitude, double longitude) {
        AddPointDialog dialog = new AddPointDialog(getContext(), latitude, longitude, new AddPointDialog.OnPointAddedListener() {
            @Override
            public void onPointAdded(String name, String description) {
                addMarker(latitude, longitude, name, description);
            }
        });
        dialog.show();
    }

    @Override
    public boolean longPressHelper(GeoPoint p) {
        return false;
    }

    private void addMarker(double latitude, double longitude, String name, String description) {
        Marker marker = new Marker(mapView);
        GeoPoint geoPoint = new GeoPoint(latitude, longitude);
        marker.setPosition(geoPoint);
        marker.setTitle(name);
        marker.setSnippet(description);
        mapView.getOverlays().add(marker);
        mapView.invalidate();
        mapView.getController().animateTo(geoPoint);
        spinnerMapLogic.addNewPoint(geoPoint.toString());
    }
}

package ru.mirea.petrakov.mireaproject.practice8;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpinnerMapSetter {

    List<String> pointsList = new ArrayList<String>(Arrays.asList("No points"));
    private Spinner spinnerPointA;
    private Spinner spinnerPointB;
    private Context context;
    public SpinnerMapSetter(Context context, Spinner spinnerPointA, Spinner spinnerPointB){
        this.spinnerPointA = spinnerPointA;
        this.spinnerPointB = spinnerPointB;
        this.context = context;

        createAdapter(spinnerPointA);
        createAdapter(spinnerPointB);

        addHoldersOnSpinners(spinnerPointA);
        addHoldersOnSpinners(spinnerPointB);
    }

    private void createAdapter(Spinner spinner){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, pointsList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void addHoldersOnSpinners(Spinner spinner){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    public void addNewPoint(String newPoint) {
        pointsList.add(newPoint);
        updateAdapter(spinnerPointA);
        updateAdapter(spinnerPointB);
    }

    private void updateAdapter(Spinner spinner) {
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) spinner.getAdapter();
        adapter.notifyDataSetChanged();
    }

    public GeoPoint[] tryGetPoints(){
        if (areSelectedIndicesDifferent()){
            return convertToGeoPoint();
        }
        return null;
    }
    private boolean areSelectedIndicesDifferent() {

        if (spinnerPointA.getSelectedItem() == spinnerPointA.getItemAtPosition(0) ||
        spinnerPointB.getSelectedItem() == spinnerPointB.getItemAtPosition(0))
            return false;

        int selectedPositionA = spinnerPointA.getSelectedItemPosition();
        int selectedPositionB = spinnerPointB.getSelectedItemPosition();
        return selectedPositionA != selectedPositionB;
    }

    public GeoPoint[] convertToGeoPoint(){
        GeoPoint[] geoPoints = new GeoPoint[2];

        String selectedItemA = (String) spinnerPointA.getSelectedItem();
        String selectedItemB = (String) spinnerPointB.getSelectedItem();

        String[] partsA = selectedItemA.split(",");
        String[] partsB = selectedItemB.split(",");

        double latitudeA = Double.parseDouble(partsA[0]);
        double longitudeA = Double.parseDouble(partsA[1]);
        double latitudeB = Double.parseDouble(partsB[0]);
        double longitudeB = Double.parseDouble(partsB[1]);

        GeoPoint geoPointA = new GeoPoint(latitudeA, longitudeA);
        GeoPoint geoPointB = new GeoPoint(latitudeB, longitudeB);

        geoPoints[0] = geoPointA;
        geoPoints[1] = geoPointB;

        return geoPoints;
    }
}

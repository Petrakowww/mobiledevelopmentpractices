package ru.mirea.petrakov.mireaproject.practice8;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Polyline;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class DistancePointerDrawer {

    private Context context;
    private MapView mapView;
    private Set<String> routeSet;

    public DistancePointerDrawer(Context context, MapView mapView) {
        this.context = context;
        this.mapView = mapView;
        this.routeSet = new HashSet<>();
    }

    @SuppressLint("MissingPermission")
    public void startDrawing(GeoPoint[] geoPoints) {
        String routeKey = generateRouteKey(geoPoints);

        if (!routeSet.contains(routeKey)) {
            FusedLocationProviderClient fusedLocation = LocationServices.
                    getFusedLocationProviderClient(context);
            fusedLocation.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location == null) {
                        Toast.makeText(context, "Location нет ссылки", Toast.LENGTH_LONG).show();
                    } else {
                        drawPath(geoPoints[0], geoPoints[1]);
                        routeSet.add(routeKey);
                    }
                }
            });
        } else {
            Toast.makeText(context, "Маршрут уже построен", Toast.LENGTH_LONG).show();
        }
    }

    private void drawPath(GeoPoint placePoint, GeoPoint userPoint) {
        Polyline polyline = new Polyline(mapView);
        polyline.addPoint(placePoint);
        polyline.addPoint(userPoint);
        // Рассчитываем расстояние между точками
        double distance = calculateDistance(placePoint, userPoint);
        // Определяем цвет линии на основе расстояния
        int color = calculateColor(distance);
        polyline.setColor(color);
        mapView.getOverlayManager().add(polyline);
    }

    private String generateRouteKey(GeoPoint[] geoPoints) {
        Arrays.sort(geoPoints, (point1, point2) -> {
            int comparison = Double.compare(point1.getLatitude(), point2.getLatitude());
            if (comparison == 0) {
                comparison = Double.compare(point1.getLongitude(), point2.getLongitude());
            }
            return comparison;
        });

        GeoPoint pointA = geoPoints[0];
        GeoPoint pointB = geoPoints[1];

        return pointA.getLatitude() + "_" + pointA.getLongitude() + "_" +
                pointB.getLatitude() + "_" + pointB.getLongitude();
    }

    private double calculateDistance(GeoPoint startPoint, GeoPoint endPoint) {
        double distance = startPoint.distanceToAsDouble(endPoint) / 1000.0;
        String message = String.format(Locale.getDefault(),
                "Дистанция между точками составляет %.2f км", distance);
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        return distance;
    }

    private int calculateColor(double distance) {
        double normalizedDistance = Math.min(distance, 100) / 100.0;
        int green = (int) (255 * (1 - normalizedDistance));
        int red = (int) (255 * normalizedDistance);
        return Color.rgb(red, green, 0);
    }
}

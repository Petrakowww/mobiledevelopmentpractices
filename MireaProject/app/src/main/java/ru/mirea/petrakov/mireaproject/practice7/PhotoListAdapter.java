package ru.mirea.petrakov.mireaproject.practice7;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.mirea.petrakov.mireaproject.R;

public class PhotoListAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private int mResource;

    public PhotoListAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            listItem = inflater.inflate(mResource, parent, false);
        }

        ImageView imageView = listItem.findViewById(R.id.imageView);
        String imageUrl = getItem(position);
        Glide.with(mContext)
                .load(imageUrl)
                .centerCrop()
                .into(imageView);

        return listItem;
    }
}

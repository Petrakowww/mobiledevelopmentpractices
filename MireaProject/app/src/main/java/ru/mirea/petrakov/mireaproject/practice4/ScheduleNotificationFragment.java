package ru.mirea.petrakov.mireaproject.practice4;

import static android.Manifest.permission.POST_NOTIFICATIONS;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.pm.PackageManager;
import android.icu.util.Calendar;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ru.mirea.petrakov.mireaproject.databinding.FragmentTaskSchedulerBinding;

public class ScheduleNotificationFragment extends Fragment {
    private int mYear, mMonth, mDay, mHour, mMinute;
    private FragmentTaskSchedulerBinding binding;

    private final int PermissionCode = 200;
    private final String TAG = this.getClass().getSimpleName();

    public ScheduleNotificationFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if	(ContextCompat.checkSelfPermission(getContext(), POST_NOTIFICATIONS) ==	PackageManager.PERMISSION_GRANTED)	{
            Log.d(TAG,"Разрешения получены");
        }	else	{
            Log.d(TAG,"Нет разрешений!");
            ActivityCompat.requestPermissions(getActivity(), new String[]{ POST_NOTIFICATIONS }, PermissionCode);
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentTaskSchedulerBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();
        Button scheduleButton = binding.buttonNotification;

        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduleNotification();
            }
        });

        return rootView;
    }

    private void scheduleNotification() {
        // Получение текущего времени и даты
        final Calendar currentTime = Calendar.getInstance();
        mYear = currentTime.get(Calendar.YEAR);
        mMonth = currentTime.get(Calendar.MONTH);
        mDay = currentTime.get(Calendar.DAY_OF_MONTH);
        mHour = currentTime.get(Calendar.HOUR_OF_DAY);
        mMinute = currentTime.get(Calendar.MINUTE);

        Log.d(TAG, String.format(Locale.getDefault(),
                "Получено текущее время и дата: %d %d %d %d %d", mYear, mMonth,
                mDay, mHour, mMinute));

        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mHour = hourOfDay;
                        mMinute = minute;

                        Log.d(TAG, String.format(Locale.getDefault(),
                                "Время установлено: %d %d", mHour, mMinute));

                        // Отображение диалога для выбора даты
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        mYear = year;
                                        mMonth = monthOfYear;
                                        mDay = dayOfMonth;

                                        Log.d(TAG, String.format(Locale.getDefault(),
                                                "Дата установлена: %d %d %d", mYear, mMonth,
                                                mDay));

                                        scheduleNotificationWorkerManager(mYear, mMonth, mDay, mHour, mMinute);
                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();
                    }
                }, mHour, mMinute, true);
        timePickerDialog.show();
    }

    private void scheduleNotificationWorkerManager(int year, int month,
                                                   int day, int hour, int minute) {
        // Создание календаря для установленной даты и времени
        Calendar notificationTime = Calendar.getInstance();
        notificationTime.set(Calendar.YEAR, year);
        notificationTime.set(Calendar.MONTH, month);
        notificationTime.set(Calendar.DAY_OF_MONTH, day);
        notificationTime.set(Calendar.HOUR_OF_DAY, hour);
        notificationTime.set(Calendar.MINUTE, minute);
        notificationTime.set(Calendar.SECOND, 0);
        Log.d(TAG, String.format(Locale.getDefault(),
                "Получено время и дата для Worker: %d %d %d %d %d", year, month,
                day, hour, minute));
        // Получение разницы во времени между текущим временем и временем уведомления
        long delay = notificationTime.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();

        // Создание WorkRequest с задержкой времени
        OneTimeWorkRequest notificationWork = new OneTimeWorkRequest.Builder(ScheduleWorker.class)
                .setInitialDelay(delay, TimeUnit.MILLISECONDS)
                .setInputData(new Data.Builder()
                        .putString("title", correctNotificationFormat(
                                binding.editTextTitle.getText().toString(),
                                "Без имени"))
                        .putString("info", correctNotificationFormat(
                                binding.editTextMultiLine.getText().toString(),
                                "Отсутствует описание"))
                        .build())
                .build();

        Log.d(TAG, String.format(Locale.getDefault(), "задержка до отправки: %d", delay));

        // Отправка запроса на выполнение работы в WorkManager
        WorkManager.getInstance(getContext()).enqueue(notificationWork);

        Toast.makeText(getContext(), "Уведомление отправлено", Toast.LENGTH_SHORT).show();
    }

    private String correctNotificationFormat(String info, String baseValue) {
        info = info.trim();
        return info.isEmpty() ? baseValue : info;
    }
}
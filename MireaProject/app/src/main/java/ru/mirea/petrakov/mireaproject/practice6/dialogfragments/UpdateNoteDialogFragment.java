package ru.mirea.petrakov.mireaproject.practice6.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.mirea.petrakov.mireaproject.practice6.NoteManager;

public class UpdateNoteDialogFragment extends BaseNoteDialogFragment {
    private EditText mOldNoteEditText;
    private EditText mNewNoteEditText;

    public UpdateNoteDialogFragment(NoteManager noteManager, ArrayAdapter<String> noteAdapter) {
        super(noteManager, noteAdapter);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        LinearLayout layout = new LinearLayout(requireContext());
        layout.setOrientation(LinearLayout.VERTICAL);

        mOldNoteEditText = new EditText(requireContext());
        mOldNoteEditText.setHint("Введите старую заметку");
        layout.addView(mOldNoteEditText);

        mNewNoteEditText = new EditText(requireContext());
        mNewNoteEditText.setHint("Введите новую заметку");
        layout.addView(mNewNoteEditText);

        builder.setView(layout)
                .setTitle("Обновить заметку")
                .setPositiveButton("Обновить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String oldNoteText = mOldNoteEditText.getText().toString().trim();
                        String newNoteText = mNewNoteEditText.getText().toString().trim();

                        if (!oldNoteText.isEmpty() && !newNoteText.isEmpty()) {
                            if (noteManager.getAllNotes().contains(oldNoteText)) {
                                noteManager.updateNote(oldNoteText, newNoteText);
                                showToast("Заметка обновлена");
                            } else {
                                showToast("Старая заметка не существует");
                            }
                        } else {
                            showToast("Введите старую и новую заметку");
                        }
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }
}

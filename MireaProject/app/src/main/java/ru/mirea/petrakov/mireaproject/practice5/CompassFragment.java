package ru.mirea.petrakov.mireaproject.practice5;

import static android.content.Context.SENSOR_SERVICE;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.hardware.SensorManager;

import java.util.Locale;

import ru.mirea.petrakov.mireaproject.databinding.FragmentCompassBinding;

public class CompassFragment extends Fragment implements SensorEventListener {
    private final String TAG = this.getClass().getSimpleName();
    private FragmentCompassBinding binding;
    private ImageView compassDynamic;
    private TextView textViewDegree;
    private float currentDegree = 0;
    private SensorManager sensorManager;
    private long timeRotateAnimation = 210;

    public CompassFragment() { }

    public static CompassFragment newInstance(String param1, String param2) {
        CompassFragment fragment = new CompassFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCompassBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();

        InitViewElements();

        return rootView;
    }

    private void InitViewElements(){
        compassDynamic = binding.imageViewCompassDynamic;
        textViewDegree = binding.textViewAngle;
        sensorManager = (SensorManager) getContext().getSystemService(SENSOR_SERVICE);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] rotationMatrix = new float[9];
        float[] orientationAngles = new float[3];
        SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values);
        SensorManager.getOrientation(rotationMatrix, orientationAngles);

        // -180 и 180
        float degree = (float) Math.toDegrees(orientationAngles[0]);

        // Если отрицательный
        if (degree < 0) {
            degree += 360;
        }

        textViewDegree.setText(String.format(Locale.getDefault(),
                "Поворот от севера: %f", degree));

        Log.d(TAG, Float.toString(degree));
        RotateAnimation rotateAnimation = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setDuration(timeRotateAnimation);

        rotateAnimation.setFillAfter(true);

        compassDynamic.startAnimation(rotateAnimation);

        currentDegree = -degree;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
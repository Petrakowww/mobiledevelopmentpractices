package ru.mirea.petrakov.mireaproject.practice6.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.List;

import ru.mirea.petrakov.mireaproject.practice6.NoteManager;

public abstract class BaseNoteDialogFragment extends DialogFragment {
    protected NoteManager noteManager;

    protected ArrayAdapter<String> noteAdapter;

    public BaseNoteDialogFragment(NoteManager noteManager, ArrayAdapter<String> noteAdapter) {
        this.noteManager = noteManager;
        this.noteAdapter = noteAdapter;
    }

    @NonNull
    @Override
    public abstract Dialog onCreateDialog(@Nullable Bundle savedInstanceState);

    protected void showToast(String message) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        List<String> allNotes = noteManager.getAllNotes();
        noteAdapter.clear();
        noteAdapter.addAll(allNotes);
        noteAdapter.notifyDataSetChanged();
    }
}
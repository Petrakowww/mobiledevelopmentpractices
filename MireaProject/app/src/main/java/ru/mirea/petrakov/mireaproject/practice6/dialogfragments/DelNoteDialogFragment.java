package ru.mirea.petrakov.mireaproject.practice6.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.mirea.petrakov.mireaproject.practice6.NoteManager;

public class DelNoteDialogFragment extends BaseNoteDialogFragment {
    private EditText mNoteEditText;

    public DelNoteDialogFragment(NoteManager noteManager, ArrayAdapter<String> noteAdapter) {
        super(noteManager, noteAdapter);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        mNoteEditText = new EditText(requireContext());
        mNoteEditText.setHint("Введите название заметки");

        builder.setView(mNoteEditText)
                .setTitle("Удалить заметку")
                .setPositiveButton("Удалить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String noteText = mNoteEditText.getText().toString().trim();
                        Boolean isWasDeleted = false;
                        if (!noteText.isEmpty()) {
                            if (noteManager.getAllNotes().contains(noteText)) {
                                noteManager.deleteNote(noteText);
                                isWasDeleted = true;
                            }
                        }
                        if (!isWasDeleted) {
                            showToast("Такой заметки не существует");
                        }
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }
}

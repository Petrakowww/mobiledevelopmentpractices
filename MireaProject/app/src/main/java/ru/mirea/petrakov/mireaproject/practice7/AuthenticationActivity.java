package ru.mirea.petrakov.mireaproject.practice7;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import ru.mirea.petrakov.mireaproject.MainActivity;
import ru.mirea.petrakov.mireaproject.R;
import ru.mirea.petrakov.mireaproject.databinding.ActivityAuthBinding;

public class AuthenticationActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getSimpleName();

    private ActivityAuthBinding binding;

    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        binding = ActivityAuthBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        mAuth = FirebaseAuth.getInstance();
        binding.buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(binding.editTextAuth.getText().toString(),
                        binding.editTextPassword.getText().toString());
            }
        });
        binding.buttonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount(binding.editTextAuth.getText().toString(),
                        binding.editTextPassword.getText().toString());
            }
        });
    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure",
                                    task.getException());
                            Toast.makeText(AuthenticationActivity.this,
                                    "Ошибка создания аккаунта",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(AuthenticationActivity.this,
                    "Пожалуйста введите заполненные данные",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(AuthenticationActivity.this, "Ошибка " +
                                            "аунтификации, возможно вы ввели некорректный пароль или почту",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = binding.editTextAuth.getText().toString();
        if (TextUtils.isEmpty(email) || !email.contains("@")) {
            binding.editTextAuth.setError("Введите корректный почтовый адрес");
            valid = false;
        } else {
            binding.editTextAuth.setError(null);
        }

        String password = binding.editTextPassword.getText().toString();
        if (TextUtils.isEmpty(password) || password.length() < 6) {
            binding.editTextPassword.setError("Пароль должен содержать по крайней мере 6 символов");
            valid = false;
        } else {
            binding.editTextPassword.setError(null);
        }

        return valid;
    }

    private void TransitionToMainActivity() {
        binding.editTextPassword.setText(null);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    private void updateUI(FirebaseUser user) {
        if (user != null){
            TransitionToMainActivity();
        }
    }
}

package ru.mirea.petrakov.mireaproject.practice6.dialogfragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import ru.mirea.petrakov.mireaproject.practice6.NoteManager;

public class DelAllNotesDialogFragment extends BaseNoteDialogFragment {

    public DelAllNotesDialogFragment(NoteManager noteManager, ArrayAdapter<String> noteAdapter) {
        super(noteManager, noteAdapter);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        builder.setMessage("Вы уверены, что хотите удалить все заметки?")
                .setTitle("Удалить все заметки")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        noteManager.deleteAllNotes();
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }
}

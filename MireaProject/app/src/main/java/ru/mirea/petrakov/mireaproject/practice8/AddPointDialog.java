package ru.mirea.petrakov.mireaproject.practice8;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ru.mirea.petrakov.mireaproject.R;

public class AddPointDialog extends Dialog {

    private EditText nameEditText, descriptionEditText;
    private Button saveButton;
    private double latitude, longitude;
    private OnPointAddedListener listener;

    public interface OnPointAddedListener {
        void onPointAdded(String name, String description);
    }

    public AddPointDialog(Context context, double latitude, double longitude, OnPointAddedListener listener) {
        super(context);
        this.latitude = latitude;
        this.longitude = longitude;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_point);
        nameEditText = findViewById(R.id.edit_text_name);
        descriptionEditText = findViewById(R.id.edit_text_description);
        saveButton = findViewById(R.id.button_save);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString();
                String description = descriptionEditText.getText().toString();

                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(getContext(), "Введите имя для точки",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                listener.onPointAdded(name, description);
                dismiss();
            }
        });
    }
}

package ru.mirea.petrakov.mireaproject.practice6;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import ru.mirea.petrakov.mireaproject.R;
import ru.mirea.petrakov.mireaproject.databinding.FragmentFileBinding;
import ru.mirea.petrakov.mireaproject.databinding.FragmentProfileBinding;
import ru.mirea.petrakov.mireaproject.practice6.dialogfragments.AddNoteDialogFragment;
import ru.mirea.petrakov.mireaproject.practice6.dialogfragments.BaseNoteDialogFragment;
import ru.mirea.petrakov.mireaproject.practice6.dialogfragments.DelAllNotesDialogFragment;
import ru.mirea.petrakov.mireaproject.practice6.dialogfragments.DelNoteDialogFragment;
import ru.mirea.petrakov.mireaproject.practice6.dialogfragments.UpdateNoteDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FileFragment extends Fragment {
    private NoteManager mNoteManager;
    private ArrayAdapter<String> arrayAdapter;
    private FragmentFileBinding binding;

    private Button buttonAdd, buttonDelete, buttonUpdate, buttonDelAll;

    private ListView notesListView;
    public FileFragment() {
    }
    public static FileFragment newInstance(String param1, String param2) {
        FileFragment fragment = new FileFragment();;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNoteManager = new NoteManager(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFileBinding.inflate(inflater, container, false);

        initElements();

        List<String> allNotes = mNoteManager.getAllNotes();

        arrayAdapter = new ArrayAdapter<>(requireContext(),
                android.R.layout.simple_list_item_1, allNotes);

        notesListView.setAdapter(arrayAdapter);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddNoteDialog();
            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDelNoteDialog();
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUpdateNoteDialog();
            }
        });

        buttonDelAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDelAllNoteDialog();
            }
        });
    }

    private void openNoteDialog(BaseNoteDialogFragment dialogFragment) {
        dialogFragment.show(getParentFragmentManager(), dialogFragment.getClass().getSimpleName());
    }

    private void openAddNoteDialog() {
        openNoteDialog(new AddNoteDialogFragment(mNoteManager, arrayAdapter));
    }

    private void openDelNoteDialog() {
        openNoteDialog(new DelNoteDialogFragment(mNoteManager, arrayAdapter));
    }

    private void openUpdateNoteDialog(){
        openNoteDialog(new UpdateNoteDialogFragment(mNoteManager, arrayAdapter));
    }

    private void openDelAllNoteDialog(){
        openNoteDialog(new DelAllNotesDialogFragment(mNoteManager, arrayAdapter));
    }

    private void initElements(){
        buttonAdd = binding.buttonAdd;
        buttonDelete = binding.buttonDel;
        buttonUpdate = binding.buttonUpdate;
        buttonDelAll = binding.buttonDelAll;
        notesListView = binding.ListNotes;
    }
}
package ru.mirea.petrakov.mireaproject.practice7;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import ru.mirea.petrakov.mireaproject.R;
import ru.mirea.petrakov.mireaproject.databinding.FragmentHttpBinding;

public class HTTPFragment extends Fragment {

    private EditText searchText;
    private ListView searchResultsListView;
    private Button searchButton;
    private FragmentHttpBinding binding;
    private String resultFilters = "";
    private HashMap<Spinner, Integer> contentFilterSpinnersMap = new HashMap<>();

    private final String TAG = this.getClass().getSimpleName();
    public HTTPFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHttpBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();
        initFields();
        return rootView;
    }

    private void initFields(){
        searchText = binding.editTextSearcHttp;
        searchResultsListView = binding.ListImages;
        searchButton = binding.buttonGetImages;
        contentFilterSpinnersMap = new HashMap<>();
        contentFilterSpinnersMap.put(binding.orientation, R.array.content_orientation_options);
        contentFilterSpinnersMap.put(binding.color, R.array.content_colors_options);
        contentFilterSpinnersMap.put(binding.orderBy, R.array.content_order_by_options);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<Spinner> spinnerList = new ArrayList<>(contentFilterSpinnersMap.keySet());

        for (int i = 0; i < spinnerList.size(); i++) {
            Spinner spinner = spinnerList.get(i);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    StringBuilder filterBuilder = new StringBuilder();

                    for (Map.Entry<Spinner, Integer> entry : contentFilterSpinnersMap.entrySet()) {
                        Spinner currentSpinner = entry.getKey();
                        int arrayResource = entry.getValue();

                        int selectedPosition = currentSpinner.getSelectedItemPosition();

                        String selectedItem = getResources().getStringArray(arrayResource)[selectedPosition];

                        if (!selectedItem.equals("No filter")) {
                            String filterName = getResources().getResourceEntryName(currentSpinner.getId());
                            filterBuilder.append("&").append(filterName).append("=").append(selectedItem);
                        }
                    }

                    resultFilters = filterBuilder.toString();
                    Log.d(TAG, resultFilters);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                }
            });
        }
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = searchText.getText().toString().trim();
                if (!query.isEmpty()) {
                    new SearchPhotosTask().execute(query, resultFilters);
                } else {
                    Toast.makeText(getContext(), "Введите то, что вы хотите найти",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class SearchPhotosTask extends AsyncTask<String, Void, List<String>> {
        @Override
        protected List<String> doInBackground(String... params) {
            String query = params[0];
            String filt = params[1];

            Log.d(TAG, query + " " + filt);
            String unsplashUrl = "https://api.unsplash.com/search/photos" +
                    "?query=" + query + "&client_id=7_RsiDvBYEdobEEGddZ-ovRCSfGxwyKNF9a4w4o5csU" +
                    filt;
            Log.d(TAG, unsplashUrl);
            List<String> photoUrls = new ArrayList<>();
            try {
                URL url = new URL(unsplashUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                Scanner scanner = new Scanner(inputStream);
                StringBuilder builder = new StringBuilder();
                while (scanner.hasNext()) {
                    builder.append(scanner.nextLine());
                }
                String jsonString = builder.toString();
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONArray results = jsonObject.getJSONArray("results");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject photoObject = results.getJSONObject(i);
                    String photoUrl = photoObject.getJSONObject("urls").getString("regular");
                    Log.d(TAG, photoUrl);
                    photoUrls.add(photoUrl);
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return photoUrls;
        }


        @Override
        protected void onPostExecute(List<String> photoUrls) {
            super.onPostExecute(photoUrls);
            PhotoListAdapter adapter = new PhotoListAdapter(getContext(), R.layout.layoutimageview, photoUrls);
            searchResultsListView.setAdapter(adapter);
        }


    }
}
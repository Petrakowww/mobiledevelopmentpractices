package ru.mirea.petrakov.mireaproject.practice6;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class NoteManager {
    private Context mContext;

    public NoteManager(Context context) {
        mContext = context;
    }

    public void addNote(String note) {
        try {
            FileOutputStream fos = mContext.openFileOutput("notes.txt", Context.MODE_APPEND);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
            writer.write(note + "\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteNote(String note) {
        List<String> notes = getAllNotes();
        notes.remove(note);
        saveNotes(notes);
    }

    public void updateNote(String oldNote, String newNote) {
        List<String> notes = getAllNotes();
        int index = notes.indexOf(oldNote);
        if (index != -1) {
            notes.set(index, newNote);
            saveNotes(notes);
        }
    }

    private void saveNotes(List<String> notes) {
        try {
            FileOutputStream fos = mContext.openFileOutput("notes.txt", Context.MODE_PRIVATE);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
            for (String note : notes) {
                writer.write(note + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getAllNotes() {
        List<String> notes = new ArrayList<>();
        try {
            FileInputStream fis = mContext.openFileInput("notes.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = reader.readLine()) != null) {
                notes.add(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return notes;
    }

    public void deleteAllNotes() {
        mContext.deleteFile("notes.txt");
    }
}

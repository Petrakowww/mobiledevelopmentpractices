package ru.mirea.petrakov.mireaproject.practice5;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import ru.mirea.petrakov.mireaproject.databinding.FragmentCollageBinding;

public class CollageFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();
    private FragmentCollageBinding binding;
    private ImageView lastClickedImageView;
    private ActivityResultLauncher<String> requestPermissionLauncher;
    private ActivityResultLauncher<Intent> captureImageLauncher;
    public CollageFragment() { }

    public static CollageFragment newInstance(String param1, String param2) {
        CollageFragment fragment = new CollageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), new ActivityResultCallback<Boolean>() {
            @Override
            public void onActivityResult(Boolean isGranted) {
                if (isGranted) {
                    dispatchTakePictureIntent();
                } else {
                    new AlertDialog.Builder(requireContext())
                            .setMessage("Доступ к камере обязателен для коллажа. Хотите ли вы предоставить доступ к камере?")
                            .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .show();
                }
            }
        });

        captureImageLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == getActivity().RESULT_OK) {
                    Bundle extras = result.getData().getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ImageView imageView = getSelectedImageView();
                    imageView.setImageBitmap(imageBitmap);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCollageBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();

        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
        }

        ConstraintLayout constraintLayout = binding.constraintL;

        if (constraintLayout != null) {
            for (int i = 0; i < constraintLayout.getChildCount(); i++) {
                View view = constraintLayout.getChildAt(i);
                Log.d(TAG, view.toString());
                if (view instanceof ImageButton) {
                    ImageButton imageButton = (ImageButton) view;
                    Log.d(TAG, imageButton.toString());
                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG, "click!");
                            lastClickedImageView = (ImageView) v;
                            dispatchTakePictureIntent();
                        }
                    });
                }
            }
        }

        return rootView;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.d(TAG, "Попыка вызывать камеру!");
        captureImageLauncher.launch(takePictureIntent);
    }

    private ImageView getSelectedImageView() {
        return lastClickedImageView;
    }
}
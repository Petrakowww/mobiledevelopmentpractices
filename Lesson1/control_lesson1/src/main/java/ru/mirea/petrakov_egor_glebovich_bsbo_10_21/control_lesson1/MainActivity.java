package ru.mirea.petrakov_egor_glebovich_bsbo_10_21.control_lesson1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        TextView textView = findViewById(R.id.HeaderNewLife);
        textView.setText("New text - Petrakov Egor");
        Button button = findViewById(R.id.button4);
        button.setText("MireaButton");
    }
}
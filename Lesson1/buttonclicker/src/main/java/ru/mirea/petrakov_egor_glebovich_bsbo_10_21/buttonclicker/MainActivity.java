package ru.mirea.petrakov_egor_glebovich_bsbo_10_21.buttonclicker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private CheckBox _checkBox;
    private Button _buttonWhoAmI;
    private TextView _studentNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        InitViewObjects();
        _buttonWhoAmI.setOnClickListener(new ButtonListener());
    }
    private void InitViewObjects(){
        _checkBox = findViewById(R.id.checkBox);
        _buttonWhoAmI = findViewById(R.id.WhoAmI);
        _studentNumber = findViewById(R.id.id_student);
    }
    public void OnMyButtonClick(View view){
        ChangeState(false, "Это сделал не я!");
    }
    class ButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            ChangeState(true, "Мой номер по списку №19");
        }
    }
    private void ChangeState(boolean state, String text){
        _checkBox.setChecked(state);
        _studentNumber.setText(text);
    }
}




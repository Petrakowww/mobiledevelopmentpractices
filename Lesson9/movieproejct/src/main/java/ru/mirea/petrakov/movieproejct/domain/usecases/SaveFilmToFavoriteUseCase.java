package ru.mirea.petrakov.movieproejct.domain.usecases;

import ru.mirea.petrakov.movieproejct.domain.models.Movie;
import ru.mirea.petrakov.movieproejct.domain.repository.MovieRepository;

public class SaveFilmToFavoriteUseCase {
    private MovieRepository movieRepository;

    public SaveFilmToFavoriteUseCase(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public boolean execute(Movie movie){
        return movieRepository.saveMovie(movie);
    }
}
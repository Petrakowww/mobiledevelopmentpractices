package ru.mirea.petrakov.movieproejct.data.repository;

import android.content.Context;
import android.content.SharedPreferences;

import ru.mirea.petrakov.movieproejct.domain.models.Movie;
import ru.mirea.petrakov.movieproejct.domain.repository.MovieRepository;

public class MovieRepositoryImpl implements MovieRepository {
    private Context context;
    private final String defaultValue = "No favorite film!";
    public MovieRepositoryImpl(Context context){
        this.context = context;
    }

    private int id = 0;
    @Override
    public boolean saveMovie(Movie movie) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("film-data",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("favourite-film", movie.getName());
        boolean isSaved = editor.commit();

        if (isSaved) {
            String savedFilm = sharedPreferences.getString("favourite-film", defaultValue);
            return movie.getName().equals(savedFilm);
        }
        return false;
    }

    @Override
    public Movie getMovie() {
        String film = context.getSharedPreferences("film-data", Context.MODE_PRIVATE).
                getString("favourite-film", defaultValue);
        return new Movie(id, film);
    }
}

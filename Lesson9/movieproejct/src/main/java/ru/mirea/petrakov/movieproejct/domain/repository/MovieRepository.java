package ru.mirea.petrakov.movieproejct.domain.repository;

import ru.mirea.petrakov.movieproejct.domain.models.Movie;

public interface MovieRepository {
    public boolean saveMovie(Movie movie);
    public Movie getMovie();
}

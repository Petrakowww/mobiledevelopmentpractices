package ru.mirea.petrakov.movieproejct.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ru.mirea.petrakov.movieproejct.R;
import ru.mirea.petrakov.movieproejct.data.repository.MovieRepositoryImpl;
import ru.mirea.petrakov.movieproejct.domain.models.Movie;
import ru.mirea.petrakov.movieproejct.domain.repository.MovieRepository;
import ru.mirea.petrakov.movieproejct.domain.usecases.GetFavoriteFilmUseCase;
import ru.mirea.petrakov.movieproejct.domain.usecases.SaveFilmToFavoriteUseCase;

public class MainActivity extends AppCompatActivity {

    private EditText favoriteFilmEditText;
    private TextView favoriteFilmTextView;

    private Button setFavoriteFilmButton;
    private Button getFavoriteFilmButton;

    private MovieRepository movieRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initalizationUIComponents();
        initalizationUIEvents();
    }

    private void initalizationUIComponents() {
        favoriteFilmEditText = findViewById(R.id.writeFavouriteFilmEditText);
        favoriteFilmTextView = findViewById(R.id.showFilmtextView);
        setFavoriteFilmButton = findViewById(R.id.saveFavouriteButton);
        getFavoriteFilmButton = findViewById(R.id.showFavoriteFilmButton);

        movieRepository = new MovieRepositoryImpl(getApplicationContext());
    }

    private void initalizationUIEvents() {
        setFavoriteFilmButton.setOnClickListener(v -> {
            Boolean result = new SaveFilmToFavoriteUseCase(movieRepository).execute(new Movie(2,
                    favoriteFilmEditText.getText().toString()));
            favoriteFilmTextView.setText(String.format("Save result %s", result));
        });

        getFavoriteFilmButton.setOnClickListener(v -> {
            Movie movie = new GetFavoriteFilmUseCase(movieRepository).execute();
            favoriteFilmTextView.setText((String.format("Save result %s", movie.getName())));
        });
    }
}
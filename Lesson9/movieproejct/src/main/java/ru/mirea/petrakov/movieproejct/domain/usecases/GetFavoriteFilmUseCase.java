package ru.mirea.petrakov.movieproejct.domain.usecases;

import ru.mirea.petrakov.movieproejct.domain.models.Movie;
import ru.mirea.petrakov.movieproejct.domain.repository.MovieRepository;

public class GetFavoriteFilmUseCase {
    private MovieRepository movieRepositoryInterface;

    public GetFavoriteFilmUseCase(MovieRepository movieRepositoryInterface) {
        this.movieRepositoryInterface = movieRepositoryInterface;
    }

    public Movie execute(){
        return movieRepositoryInterface.getMovie();
    }
}

package ru.mirea.petrakov.looper;

import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.os.Handler;

import androidx.annotation.NonNull;

import java.util.Locale;
import java.util.Objects;

public class MyLooper extends Thread{
    public Handler mHandler;
    private Handler mainThreadHandler;

    private final String TAG = MyLooper.class.getSimpleName();
    public MyLooper(Handler mainThreadHandler){
        this.mainThreadHandler = mainThreadHandler;
    }

    @Override
    public void run() {
        Log.d(TAG, "running mylooper");
        Looper.prepare();

        mHandler = new Handler(Objects.requireNonNull(Looper.myLooper())){
            @Override
            public void handleMessage(@NonNull Message message)	{
                String data	= message.getData().getString("KEY");
                String workPlace = message.getData().getString("placeOfWork");
                int age = message.getData().getInt("age");


                Log.d(TAG, String.format(Locale.getDefault(),
                        "MyLooper get message: %s %s %d", data, workPlace, age));

                int	count =	data.length();
                Message	messageData	=	new	Message();
                Bundle bundle =	new	Bundle();
                bundle.putString("result",	String.format(Locale.getDefault(),
                        "The number of letters in the word %s is	%d\nВозраст: %d\n" +
                                "Должность: %s", data, count, age, workPlace));
                messageData.setData(bundle);
                mainThreadHandler.postDelayed(new Runnable() {
                                                  @Override
                                                  public void run() {
                                                      mainThreadHandler.sendMessage(messageData);
                                                  }
                                              }
                        ,100 * age);
            }
        };

        Looper.loop();
    }
}

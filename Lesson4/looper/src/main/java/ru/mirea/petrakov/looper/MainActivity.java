package ru.mirea.petrakov.looper;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;

import java.util.Locale;

import ru.mirea.petrakov.looper.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private final String TAG = MyLooper.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Handler mainThreadHandler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg){
                Log.d(MainActivity.class.getSimpleName(), String.format(
                        Locale.getDefault(), "Task execute. This is result: %s",
                        msg.getData().getString("result")
                ));
            };
        };

        MyLooper looper = new MyLooper(mainThreadHandler);
        looper.start();
        binding.editTextNumber.setText("Мой номер по списку №19");
        binding.buttonLooper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message	msg	= Message.obtain();
                Bundle bundle = new	Bundle();
                int age = 0;
                try {
                    age = Integer.parseInt(binding.editTextAge.getText().toString());
                    if (age <= 0){
                        return;
                    }
                } catch (NumberFormatException e) {
                    return;
                }
                bundle.putInt("age", age);
                String placeOfWork = binding.editTextWork.getText().toString();
                bundle.putString("placeOfWork", placeOfWork);
                bundle.putString("KEY", "mirea");
                msg.setData(bundle);
                looper.mHandler.sendMessage(msg);
            }
        });
    }
}
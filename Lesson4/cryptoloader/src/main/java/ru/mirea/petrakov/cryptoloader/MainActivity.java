package ru.mirea.petrakov.cryptoloader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import ru.mirea.petrakov.cryptoloader.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks {
    public final String TAG = this.getClass().getSimpleName();
    private int loaderCounter = 0;
    private final int LoaderId = 1234;
    private ActivityMainBinding binding;
    private final Map<String, Integer> loaderMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    public void onClickButton(View view){
        Bundle bundle = new Bundle();
        bundle.putString(MyLoader.ARG_WORD, "mirea");
        LoaderManager.getInstance(this).initLoader(LoaderId, bundle, this);
    }

    public void onClickEncryptButton(View view){
        String inputText = binding.editTextML.getText().toString();
        if (loaderMap.containsKey(inputText)) {
            int loaderIndex = loaderMap.get(inputText);
            Bundle bundle = new Bundle();
            bundle.putString(MyLoader.ARG_WORD, inputText);
            LoaderManager.getInstance(this).initLoader(loaderIndex, bundle, this);
        } else {
            Bundle bundle = new Bundle();
            SecretKey key = generateKey();
            bundle.putByteArray("key", key.getEncoded());
            bundle.putByteArray(MyLoader.ARG_WORD, encryptMsg(inputText, key));
            int id = ++loaderCounter + LoaderId;
            LoaderManager.getInstance(this).initLoader(id, bundle, this);
            loaderMap.put(inputText, id);
        }
    }

    @NonNull
    @Override
    public Loader onCreateLoader(int id, @Nullable Bundle args) {
        if (id >= LoaderId){
            Toast.makeText(this, "onCreateLoader: " + id, Toast.LENGTH_SHORT).show();
            return new MyLoader(this, args);
        }
        throw new InvalidParameterException("invalid loader id");
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object data) {
        if (loader.getId() >= LoaderId){
            Log.d(TAG, "onLoadFinished: " + data);
            Toast.makeText(this, "onLoadFinished: " + data, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        Log.d(TAG, "onLoaderReset");
    }

    public static SecretKey generateKey(){
        try{
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed("petrakov egor bsbo-10-21".getBytes());
            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(256, sr);
            return new SecretKeySpec((kg.generateKey()).getEncoded(), "AES");
        }catch (NoSuchAlgorithmException e){
            throw new RuntimeException(e);
        }
    }

    public static byte[] encryptMsg(String message, SecretKey secret) {
        Cipher cipher =	null;
        try	{
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            return cipher.doFinal(message.getBytes());
        } catch	(NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                       BadPaddingException | IllegalBlockSizeException e)	{
            throw new RuntimeException(e);
        }
    }
}
package ru.mirea.petrakov.cryptoloader;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MyLoader extends AsyncTaskLoader<String> {
    public final static String ARG_WORD = "word";
    private String result;

    private byte[] cipherText;
    private byte[] keyBytes;
    public MyLoader(@NonNull Context context, Bundle args) {
        super(context);
        if (args != null){
            result = args.getString(ARG_WORD);
            cipherText = args.getByteArray(ARG_WORD);
            keyBytes = args.getByteArray("key");
        }

    }

    @Override
    protected void onStartLoading(){
        super.onStartLoading();
        forceLoad();
    }

    @Nullable
    @Override
    public String loadInBackground() {
        SystemClock.sleep(5000);
        if (cipherText != null && keyBytes != null) {
            SecretKey originalKey = new SecretKeySpec(keyBytes, 0, keyBytes.length, "AES");
            result = decryptMsg(cipherText, originalKey);
            return result;
        } else {
            return result;
        }
    }

    public	static	String	decryptMsg(byte[]	cipherText,	SecretKey secret){
        try	{
            Cipher cipher =	Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secret);
            return new String(cipher.doFinal(cipherText));
        }	catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
                       | BadPaddingException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }
}

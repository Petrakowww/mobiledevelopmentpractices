package ru.mirea.petrakov.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import ru.mirea.petrakov.musicplayer.databinding.ActivityMusicPlayerBinding;

public class MusicPlayer extends AppCompatActivity {
    private static final String TAG = MusicPlayer.class.getSimpleName();
    private ActivityMusicPlayerBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMusicPlayerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.textViewEndSong.setText("2:03");
        binding.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "You start best track ever!");
            }
        });
    }
}
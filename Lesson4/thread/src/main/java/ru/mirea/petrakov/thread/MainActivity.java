package ru.mirea.petrakov.thread;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Arrays;
import java.util.Locale;

import ru.mirea.petrakov.thread.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private int counter = 0;
    private static final String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBinding();
        getInfoAboutMainThread();
        buttonAction();
    }

    private void initBinding(){
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    private void getInfoAboutMainThread(){
        Thread mainThread = Thread.currentThread();

        binding.textViewThread.setText(String.format("Имя текущего потока: %s",
                mainThread.getName()));

        mainThread.setName("МОЙ НОМЕР ГРУППЫ: БСБО-10-21, " +
                "НОМЕР ПО СПИСКУ: 19, МОЙ ЛЮБИМЫЙ ФИЛЬМ: ОДИН ДОМА");
        binding.textViewThread.append(String.format("\nНовое имя потока: %s", mainThread.getName()));
        Log.d(TAG, String.format("Stack %s", Arrays.toString(mainThread.getStackTrace())));
        Log.d(TAG, String.format("Группа: %s", mainThread.getThreadGroup()));
    }

    private void buttonAction(){
        binding.buttonCalcClasses.setOnClickListener(new View.OnClickListener()	{
            @Override
            public void onClick(View v){
                new	Thread(new Runnable()	{
                    public void run()	{
                        int	numberThread = counter++;
                        Log.d("ThreadProject",	String.format("Запущен поток №%d студентом " +
                                "группы %s номер по списку №%d", numberThread, "БСБО-10-21", 19));
                        long endTime = System.currentTimeMillis() +	20 * 1000;
                        while (System.currentTimeMillis() <	endTime) {
                            synchronized (this) {
                                try	{
                                    wait(endTime	- System.currentTimeMillis());
                                    Log.d(TAG, String.format("Endtime: %s", endTime));
                                }catch (Exception e) {
                                    throw new RuntimeException(e);
                                }
                            }
                            Log.d("ThreadProject","Выполнен поток № " + numberThread);
                        }
                    }
                }).start();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        float days, lessons = 0;

                        try {
                            days = Float.parseFloat(binding.editTextDays.getText().toString());
                            lessons = Float.parseFloat(binding.editTextClasses.getText().toString());
                            if (days == 0){
                                return;
                            }
                        } catch (NumberFormatException e){
                            return;
                        }
                        final float finalLessons = lessons;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                float result = finalLessons / days;
                                String newText = String.format(Locale.getDefault(), "%.2f", result);
                                binding.textViewRes.setText(newText);
                            }
                        });
                    }
                }).start();
            }
        });
    }
}
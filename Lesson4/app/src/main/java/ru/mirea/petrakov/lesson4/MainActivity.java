package ru.mirea.petrakov.lesson4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import ru.mirea.petrakov.lesson4.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater()); // с помощью статического
        // метода создаем из содержимого Layout файла View элемент

        setContentView(binding.getRoot());
        Init();
    }

    private void Init(){
        binding.textViewMirea.setText("Мой номер по списку №19");
        binding.editTextMirea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i(TAG, "Вызов по editText");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i(TAG, "Изменение текста");
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i(TAG, "Текст был изменен");
            }
        });

        binding.buttonMirea1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Кнопка мирэа с id 1 была нажата");
            }
        });

        binding.buttonMirea2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Кнопка мирэа с id 2 была нажата");
            }
        });
    }
}
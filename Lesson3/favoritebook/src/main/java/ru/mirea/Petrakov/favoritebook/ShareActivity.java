package ru.mirea.Petrakov.favoritebook;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class ShareActivity extends AppCompatActivity {
    private TextView textView;
    private EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initView();
    }

    private void initView(){
        editText = findViewById(R.id.editTextS2);
        textView = findViewById(R.id.bookFavouriteS2TV);
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            textView.setText(String.format(Locale.getDefault(),
                            "%s: %s", textView.getText(),
                            extras.getString(MainActivity.KEY))
                            );
            if (textView.getText().length() > 0){
                editText.setText(extras.getString(MainActivity.KEY));
            }
        }
    }

    public void SendRequestData(View view){
        SetNewBookName();
    }

    private void SetNewBookName(){
        String result = editText.getText().toString();
        Intent data = new Intent();
        if (result.length() > 0) {
            data.putExtra(MainActivity.USER_MESSAGE, result);
            setResult(Activity.RESULT_OK, data);
        }
        else{
            setResult(Activity.RESULT_CANCELED, data);
        }
        finish();
    }
}
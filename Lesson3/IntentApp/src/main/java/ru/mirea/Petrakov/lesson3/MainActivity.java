package ru.mirea.Petrakov.lesson3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private String SystemDateFormat(String format){
        long dateInMillis = System.currentTimeMillis();
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(dateInMillis));
    }

    private void SendToActivity(String resultString){
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("key", resultString);
        startActivity(intent);
    }

    public void onClickSendCurrentTime(View view){
        SendToActivity(SystemDateFormat("yyyy-MM-dd HH:mm:ss"));
    }
}
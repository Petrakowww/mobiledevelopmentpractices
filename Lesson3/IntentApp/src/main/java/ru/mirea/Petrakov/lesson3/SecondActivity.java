package ru.mirea.Petrakov.lesson3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SecondActivity extends AppCompatActivity {
    private TextView dateStudent;
    private final int studentNumber = 19;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        init();
        String formatted = String.format(Locale.getDefault(),
                "Квадрат значения моего номера по списку в группе составляет %.0f, а текущее время %s",
                Math.pow(studentNumber, 2), GetTime(GetIntentInfo()));
        dateStudent.setText(formatted);
    }

    private void init(){
        dateStudent = findViewById(R.id.textView);
    }

    private String GetTime(String dateString){
        return dateString.substring(11, 19);
    }

    private String GetIntentInfo(){
        return getIntent().getStringExtra("key");
    }


}
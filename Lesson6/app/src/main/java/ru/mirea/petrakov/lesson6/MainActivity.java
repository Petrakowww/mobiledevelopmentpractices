package ru.mirea.petrakov.lesson6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kotlin.jvm.internal.PropertyReference0Impl;
import ru.mirea.petrakov.lesson6.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    private EditText studentGroup, numberOfGroup, film;

    private Button button;

    private final String _GROUP_ = "NUMBER_GROUP_STUDENT";
    private final String _NUMBER_ = "NUMBER_LIST_STUDENT";
    private final String _FILM_ = "FAVOURITE_FILM";
    private final String _MIREA_SETTINGS_ = "MIREA_SETTINGS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initFields();

        GetInformation();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences(_MIREA_SETTINGS_,
                        Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = settings.edit();

                editor.putString(_GROUP_, studentGroup.getText().toString());
                editor.putString(_NUMBER_, numberOfGroup.getText().toString());
                editor.putString(_FILM_, film.getText().toString());

                editor.apply();
            }
        });
    }

    private void initFields(){
        studentGroup = binding.numberGroupPlText;
        numberOfGroup = binding.numberOfListGroupPLT;
        film = binding.filmPIT;
        button = binding.buttonSave;
    }

    private void GetInformation(){
        SharedPreferences sharedPref = getSharedPreferences(_MIREA_SETTINGS_, Context.MODE_PRIVATE);
        studentGroup.setText(sharedPref.getString(_GROUP_, ""));
        numberOfGroup.setText(sharedPref.getString(_NUMBER_, ""));
        film.setText(sharedPref.getString(_FILM_, ""));
    }
}
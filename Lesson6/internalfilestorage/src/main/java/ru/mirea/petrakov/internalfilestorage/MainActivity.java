package ru.mirea.petrakov.internalfilestorage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import ru.mirea.petrakov.internalfilestorage.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private EditText memorableData;
    private Button saveButton;
    private final String _MEMORABLE_DATA_RUSSIA_ = "MemorableDateRussia.txt";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initMemorableText();
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileOutputStream outputStream;
                try {
                    outputStream = openFileOutput(_MEMORABLE_DATA_RUSSIA_, Context.MODE_PRIVATE);
                    outputStream.write(memorableData.getText().toString().getBytes());
                    outputStream.close();

                    Log.d(this.getClass().getSimpleName(), "Запись успешно произведена!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initMemorableText(){
        memorableData = binding.multilineDescTextView;
        saveButton = binding.saveButton;

        memorableData.setText(getTextFromFile());
    }

    public String getTextFromFile() {
        FileInputStream fin = null;
        try {
            fin = openFileInput(_MEMORABLE_DATA_RUSSIA_);
            byte[] bytes = new byte[fin.available()];
            fin.read(bytes);
            String text = new String(bytes);
            return text;
        } catch (IOException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (fin != null){
                    fin.close();
                }
            } catch (IOException ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        return null;
    }
}
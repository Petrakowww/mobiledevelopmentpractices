The Day of the Great October Socialist Revolution

The day of the Great October Socialist Revolution is the anniversary of the October Revolution of 1917, falls on October 25 (November 7). The state holiday of Soviet Russia and the former USSR, in the last years of its existence, was celebrated for two days. It is one of the memorable days of Russia[2].

After the collapse of the USSR, most of its former republics, which became independent states, refused to celebrate the anniversary of the October Revolution. November 7 is a day off in Belarus and Transnistria. In Kyrgyzstan, November 7 and 8 are days off, but the meaning of the celebrated holiday has been changed[3].
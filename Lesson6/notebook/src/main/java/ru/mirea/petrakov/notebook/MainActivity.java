package ru.mirea.petrakov.notebook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.os.Bundle;
import android.os.Environment;
import android.renderscript.ScriptGroup;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ru.mirea.petrakov.notebook.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    private EditText nameFile;
    private EditText descriptionOfQuote;

    private Button saveButton;

    private Button loadButton;

    private final int REQUEST_CODE_PERMISSION = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initFields();

        if (!(isExternalStorageReadable() && isExternalStorageWritable())) {
            requestReadWritePermissions();
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeFileToExternalStorage(v);
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                readFileFromExternalStorage(v);
            }
        });
    }

    private void initFields(){
        nameFile = binding.editTextNameQuote;
        descriptionOfQuote = binding.descriptQuoteMultilIne;
        saveButton = binding.buttonSave;
        loadButton = binding.buttonDownload;
    }

    private void requestReadWritePermissions() {
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_PERMISSION);
    }

    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public void writeFileToExternalStorage(View view) {
        if (!isExternalStorageReadable()) {
            return;
        }

        String nameOfQuote = nameFile.getText().toString();
        String description = descriptionOfQuote.getText().toString();
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(path, nameOfQuote);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsoluteFile());
            OutputStreamWriter output = new OutputStreamWriter(fileOutputStream);
            // Запись строки в файл
            output.write(description);
            // Закрытие потока записи
            output.close();
        } catch (IOException e) {
            Log.e("ExternalStorage", "Error writing " + file, e);
        }
    }

    public void readFileFromExternalStorage(View view) {
        if (!isExternalStorageWritable()) {
            return;
        }
        String nameOfQuote = nameFile.getText().toString();

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(path, nameOfQuote);
        try {
            List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
            String stringLines = String.join("\n", lines);
            descriptionOfQuote.setText(stringLines);

            Log.w("ExternalStorage", String.format("Read from file %s successful", file.getAbsolutePath()));
        } catch (Exception e) {
            Log.e("ExternalStorage", String.format("Read from file %s failed", e.getMessage()));
        }
    }

}
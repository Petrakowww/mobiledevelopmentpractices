package ru.mirea.petrakov.httpurlconnection;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import ru.mirea.petrakov.httpurlconnection.databinding.ActivityMainBinding;

public class DownloadPageTask extends AsyncTask<String, Void, String> {

    private ActivityMainBinding binding;
    private Context context;

    private final String TAG = this.getClass().getSimpleName();

    public DownloadPageTask(ActivityMainBinding binding, Context context) {
        this.binding = binding;
        this.context = context;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            return downloadIpInfo(urls[0]);
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            JSONObject responseJson = new JSONObject(result);
            Log.d(TAG, responseJson.toString());
            if (responseJson.has("current_weather")) {
                JSONObject weatherJson = responseJson.getJSONObject("current_weather");
                Log.d(TAG, weatherJson.toString());
                binding.textView.setText(String.format("Дата и Время: %s\nТемпература: %s\nСкорость ветра: %s\nНаправление ветра: %s",
                        weatherJson.getString("time"),
                        weatherJson.getString("temperature"),
                        weatherJson.getString("windspeed"),
                        weatherJson.getString("winddirection")));
            } else {
                JSONArray sensorArray = responseJson.names();
                ArrayList<HashMap<String, Object>> arrayList = new ArrayList<>();

                for (int i = 0; i < sensorArray.length(); i++) {
                    String key = sensorArray.getString(i);
                    HashMap<String, Object> sensorData = new HashMap<>();
                    sensorData.put("Name", key);
                    sensorData.put("Value", responseJson.get(key));
                    arrayList.add(sensorData);
                }

                SimpleAdapter simpleAdapter = new SimpleAdapter(context, arrayList, android.R.layout.simple_list_item_2,
                        new String[]{"Name", "Value"}, new int[]{android.R.id.text1, android.R.id.text2});
                binding.listView.setAdapter(simpleAdapter);

                String[] location = responseJson.getString("loc").split(",");

                String path = String.format("https://api.open-meteo.com/v1/forecast?latitude=%s&longitude=%s&current_weather=true", location[0], location[1]);
                new DownloadPageTask(binding, context).execute(path);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onPostExecute(result);
    }

    private String downloadIpInfo(String address) throws IOException {
        InputStream inputStream = null;
        String data = "";
        try {
            URL url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(100000);
            connection.setConnectTimeout(100000);
            connection.setRequestMethod("GET");
            connection.setInstanceFollowRedirects(true);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                inputStream = connection.getInputStream();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                int read = 0;
                while ((read = inputStream.read()) != -1) {
                    bos.write(read); }
                bos.close();
                data = bos.toString();
            } else {
                data = connection.getResponseMessage()+". Error Code: " + responseCode;
            }
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return data;
    }
}

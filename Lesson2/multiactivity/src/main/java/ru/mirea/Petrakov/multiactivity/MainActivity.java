package ru.mirea.Petrakov.multiactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText textToSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textToSubmit = findViewById(R.id.editTextText);
    }

    public void onClickNewActivity(View view){ // Для примера оставил данное событие
        IntentSend(SecondActivity.class, "key", "MIREA - ПЕТРАКОВ ЕГОР ГЛЕБОВИЧ");
    }

    public void onClickEditNewActivity(View view){ // Для задания
        IntentSend(SecondActivity.class, "key", textToSubmit.getText().toString());
    }

    public  void IntentSend(Class<?> objClass, String key, String value){
        Intent intent = new Intent(this, objClass);
        intent.putExtra(key, value);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle saveInstanceState) {
        super.onRestoreInstanceState(saveInstanceState);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle saveInstanceState) {
        super.onSaveInstanceState(saveInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
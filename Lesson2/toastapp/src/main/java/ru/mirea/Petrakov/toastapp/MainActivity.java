package ru.mirea.Petrakov.toastapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private EditText multilineText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitElements();
        TaskExample();
    }

    private void InitElements(){
        multilineText = findViewById(R.id.editTextTextMultiLine);
    }

    private void TaskExample(){ // пример из pdf
        Toast toast = Toast.makeText(getApplicationContext(),
                "Здравствуй MIREA!",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    public void onClickCalculate(View view){
        int length = multilineText.length();
        Toast toast = Toast.makeText(
                getApplicationContext(),
                String.format(Locale.getDefault(),
                        "СТУДЕНТ № %d ГРУППА %s КОЛИЧЕСТВО СИМВОЛОВ - %d",
                        19, "БСБО-10-21", length),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.LEFT, 0, 0); //ДЛЯ ПРИМЕРА
        toast.show();
    }
}
package ru.mirea.Petrakov.individualwork;

import androidx.appcompat.app.AppCompatActivity;

import android.icu.util.Calendar;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    private Calendar calendar;
    private TextView currentDateTime;
    MyTimeDialogFragment timeDialogFragment;
    MyDateDialogFragment dateDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init(){
        calendar = Calendar.getInstance();
        currentDateTime = findViewById(R.id.currentDateTime);
        timeDialogFragment = MyTimeDialogFragment.CreateTimeDialogFragment(
                MainActivity.this, calendar, new Runnable() {
                    @Override
                    public void run() {
                        updateDisplayTime();
                    }
                }
        );

        dateDialogFragment = MyDateDialogFragment.CreateDateDialogFragment(
                MainActivity.this, calendar, new Runnable() {
                    @Override
                    public void run() {
                        updateDisplayTime();
                    }
                }
        );

    }

    public void onClickTimeDialogFragment(View view){
        timeDialogFragment.show();
        SnackbarAppearing(view, "Вы вызвали диалоговое окно - TimePickerDialog");
    }

    public void onClickDateDialogFragment(View view){
        dateDialogFragment.show();
        SnackbarAppearing(view, "Вы вызвали диалоговое окно - DatePickerDialog");
    }

    public void onClickProgressDialog(View view){
        MyProgressDialogFragment progressDialogFragment = new MyProgressDialogFragment(MainActivity.this);
        progressDialogFragment.show();
        SnackbarAppearing(view, "Вы вызвали диалоговое окно - ProgressDialogFragment");
    }

    private void updateDisplayTime() {
        currentDateTime.setText(DateUtils.formatDateTime(this,
                calendar.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_SHOW_TIME)); // Флаги для форматирования -
        // показывать дату, год и время для преобразованной строки
    }

    private void SnackbarAppearing(View view, String string){
        Snackbar.make(view, string, Snackbar.LENGTH_LONG).show();
    }
}
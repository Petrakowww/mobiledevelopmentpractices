package ru.mirea.Petrakov.individualwork;

import android.app.ProgressDialog;
import android.content.Context;

public class MyProgressDialogFragment extends ProgressDialog {
    public MyProgressDialogFragment(Context context) {
        super(context);
        setTitle("Окно прогресса");
        setProgressStyle(ProgressDialog.STYLE_SPINNER);
        setMessage("Загружаю информацию, подождите немного...");
    }
}

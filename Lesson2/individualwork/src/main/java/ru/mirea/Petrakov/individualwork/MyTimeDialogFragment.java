package ru.mirea.Petrakov.individualwork;

import android.app.TimePickerDialog;
import android.content.Context;
import android.icu.util.Calendar;
import android.widget.TimePicker;

public class MyTimeDialogFragment extends TimePickerDialog{

    public MyTimeDialogFragment(Context context, OnTimeSetListener listener,
                                int hourOfDay, int minute, boolean is24HourView) {
        super(context, listener, hourOfDay, minute, is24HourView);
    }

    public static MyTimeDialogFragment CreateTimeDialogFragment(Context context, Calendar calendar, Runnable function){
        return new MyTimeDialogFragment(
                context, new OnTimeSetListener(){

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        function.run();
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );
    }
}

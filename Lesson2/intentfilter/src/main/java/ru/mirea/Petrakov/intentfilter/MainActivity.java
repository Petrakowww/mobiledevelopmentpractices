package ru.mirea.Petrakov.intentfilter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button browseButton, infoButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitActivityElement();
        OpenBrowserMIREA();
        InfoFIO();
    }

    private void InitActivityElement(){
        browseButton = findViewById(R.id.button);
        infoButton = findViewById(R.id.button2);
    }

    // можно было и через layout (я решил освежить воспоминания)
    private void OpenBrowserMIREA(){

        browseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri address = Uri.parse("https://www.mirea.ru/");
                Intent openLinkIntent = new Intent(Intent.ACTION_VIEW, address);
                startActivity(openLinkIntent);
            }
        });
    }

    private void InfoFIO() {

        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _shareIntent = new Intent(Intent.ACTION_SEND);
                _shareIntent.setType("text/plain");
                _shareIntent.putExtra(Intent.EXTRA_SUBJECT, "MIREA");
                _shareIntent.putExtra(Intent.EXTRA_TEXT, "Петраков Егор Глебович");
                startActivity(Intent.createChooser(_shareIntent, "Моё ФИО"));
            }
        });
    }
}
package ru.mirea.Petrakov.activitylifecycle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

public class MainActivity extends AppCompatActivity {

    /*
        Вопросы:
            1. Будет ли вызван метод «onCreate» после нажатия на кнопку «Home» и возврата
            в приложение?

                Нет, не будет, поскольку:
                    Когда пользователь нажимает кнопку «Home» на устройстве, приложение переходит в фоновый режим, и метод onPause()
                    вызывается для текущей активности.
                    Когда пользователь возвращается в приложение, метод onCreate() не вызывается, если приложение просто находилось
                    в фоновом режиме и не было убито системой. Вместо этого вызывается метод onResume() для возврата к активному состоянию.

            2. Изменится ли значение поля «EditText» после нажатия на кнопку «Home» и
            возврата в приложение?

                Нет, не изменится, поскольку:
                    При возврате в приложение после нажатия кнопки «Home» значение поля EditText не изменится,
                    если пользователь не внес каких-либо изменений в поле в процессе использования других приложений.
                    После возвращения в приложение метод onResume() вызывается, и поле EditText
                    останется с тем же значением, которое было до выхода из приложения.

            3. Изменится ли значение поля «EditText» после нажатия на кнопку «Back» и
            возврата в приложение?

                Нет, не изменится, поскольку:
                    Нажатие кнопки «Back» означает переход к предыдущему состоянию или закрытие текущей активности.
                    Если пользователь нажмет кнопку «Back» в приложении и затем вернется обратно в приложение, значение поля EditText не изменится,
                    если только пользователь не внес изменения в поле перед нажатием кнопки «Back».
                    Как и в случае с кнопкой «Home», при возвращении в приложение вызывается метод onResume(),
                    и содержимое EditText останется без изменений, если пользователь не изменил его в процессе использования других приложений.
     */

    private final String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "method onCreate() was called");
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.i(TAG, "method onStart() was called");
    }

    @Override
    protected void onRestoreInstanceState(@NotNull Bundle saveInstanceState){
        super.onRestoreInstanceState(saveInstanceState);
        Log.i(TAG, "method onRestoreInstanceState(Bundle state) was called");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.i(TAG, "method onRestart() was called");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i(TAG, "method onResume() was called");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.i(TAG,"method onPause() was called");
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle saveInstanceState){
        super.onSaveInstanceState(saveInstanceState);
        Log.i(TAG, "method onSavedInstanceState(Bundle state) was called");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.i(TAG, "method onStop() was called");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.i(TAG, "method onDestroy() was called");
    }

}
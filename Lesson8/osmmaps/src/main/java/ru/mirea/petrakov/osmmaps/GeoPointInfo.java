package ru.mirea.petrakov.osmmaps;

import org.osmdroid.util.GeoPoint;

public class GeoPointInfo {
    public GeoPoint point;
    public String title;
    public String description;


    public GeoPointInfo(GeoPoint point, String title, String description) {
        this.point = point;
        this.title = title;
        this.description = description;
    }
}

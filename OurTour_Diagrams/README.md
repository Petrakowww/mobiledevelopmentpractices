# Диаграммы

## Выполнил: Петраков Егор (БСБО-10-21)
---

## Диаграмма для приложения: "Наш тур"

### Use-case диаграмма приложения

![Use-Case Diagram](./img/use-case-diagram.drawio.png)

### Диаграмма слоев приложения

![Layers Diagram](./img/application-layer-diagram.drawio.png)